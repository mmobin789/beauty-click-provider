package mobin.beautyclickprovider.fcm

import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import mobin.beautyclickprovider.utils.AppStorage

class FCMService : FirebaseMessagingService() {
    override fun onNewToken(p0: String?) {
        Log.d(javaClass.simpleName, "Refreshed token: " + p0!!)
        AppStorage.init(this)
        AppStorage.setFCM(p0)
    }
}
