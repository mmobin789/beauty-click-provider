package mobin.beautyclickprovider.viewmodels

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModel
import android.util.Log
import com.google.android.gms.maps.model.LatLng
import io.reactivex.android.schedulers.AndroidSchedulers.mainThread
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import mobin.beautyclickprovider.api.Api
import mobin.beautyclickprovider.controllers.Base
import mobin.beautyclickprovider.listeners.*
import mobin.beautyclickprovider.models.*
import mobin.beautyclickprovider.subcontrollers.BaseUI
import mobin.beautyclickprovider.utils.Utils
import java.io.File

class BeautyClickImpl : ViewModel() {
    private lateinit var disposable: Disposable


    fun addPromotion(base: Base, onApiListener: OnApiListener, freelancerPromotion: FreelancerPromotion) {
        val services = MutableLiveData<ApiResponse>()
        disposable = Api.addPromotion(freelancerPromotion).subscribeOn(Schedulers.io())
                .observeOn(mainThread())
                .subscribe({ apiResponse ->
                    services.postValue(apiResponse)
                    services.observe(base, Observer {
                        onApiListener.onApiResponse(it!!)
                    })

                }, {
                    onApiListener.onApiError(it.toString())
                }
                )


    }

    fun deletePromotion(id: Int, baseUI: BaseUI, onApiListener: OnApiListener) {
        val services = MutableLiveData<ApiResponse>()
        disposable = Api.deletePromotion(id).subscribeOn(Schedulers.io())
                .observeOn(mainThread())
                .subscribe({ apiResponse ->
                    services.postValue(apiResponse)
                    services.observe(baseUI, Observer {
                        onApiListener.onApiResponse(it!!)
                    })

                }, {
                    onApiListener.onApiError(it.toString())
                }
                )


    }

    fun getServices(baseUI: BaseUI, onServicesListener: OnServicesListener) {
        val services = MutableLiveData<ProviderService>()
        disposable = Api.getServices().subscribeOn(Schedulers.io())
                .observeOn(mainThread())
                .subscribe({
                    val jsonString = Api.gson.toJson(it)
                    Log.i("ProviderServicesAPI", jsonString)
                    services.postValue(it)
                    services.observe(baseUI, Observer {
                        onServicesListener.onServicesListed(it)
                    })

                }, {
                    Log.e("ProviderServicesAPI", it.toString())
                    onServicesListener.onApiError(it.toString())
                }
                )


    }

    fun getPromotions(baseUI: BaseUI, onApiListener: OnApiListener) {
        val services = MutableLiveData<ApiResponse>()
        disposable = Api.getPromotions().subscribeOn(Schedulers.io())
                .observeOn(mainThread())
                .subscribe({ apiResponse ->
                    services.postValue(apiResponse)
                    services.observe(baseUI, Observer {
                        onApiListener.onApiResponse(it!!)
                        disposable.dispose()
                    })

                }, {
                    it.printStackTrace()
                    onApiListener.onApiError(it.toString())
                }
                )


    }

    fun getServices(base: Base, onServicesListener: OnServicesListener) {
        val services = MutableLiveData<ProviderService>()
        disposable = Api.getServices().subscribeOn(Schedulers.io())
                .observeOn(mainThread())
                .subscribe({
                    val jsonString = Api.gson.toJson(it)
                    Log.i("ProviderServicesAPI", jsonString)
                    services.postValue(it)
                    services.observe(base, Observer {
                        onServicesListener.onServicesListed(it)
                        disposable.dispose()
                    })

                }, {
                    Log.e("ProviderServicesAPI", it.toString())
                    onServicesListener.onApiError(it.toString())
                    disposable.dispose()
                }
                )


    }

    fun getReviews(baseUI: BaseUI, onReviewsListener: OnReviewsListener) {
        val reviews = MutableLiveData<List<Review>>()
        disposable = Api.getReviews().observeOn(mainThread()).subscribeOn(Schedulers.io()).subscribe(
                {
                    Log.i("ReviewsAPI", Api.gson.toJson(it))
                    reviews.postValue(it)
                    reviews.observe(baseUI, Observer {
                        onReviewsListener.onReviews(it)
                    })
                }, {
            Log.e("ReviewsAPI", it.toString())
            onReviewsListener.onApiError(it.toString())
        }
        )


    }

    fun getJobs(baseUI: BaseUI, onJobsListener: OnJobsListener) {
        val jobs = MutableLiveData<List<Job>>()
        disposable = Api.getJobs().subscribeOn(Schedulers.io())
                .observeOn(mainThread())
                .subscribe(
                        {
                            jobs.postValue(it)
                            jobs.observe(baseUI, Observer {
                                Log.i("OrdersAPI", Api.gson.toJson(it))
                                onJobsListener.onJobsListed(it)
                            })
                        }, {
                    Log.e("OrdersAPI", it.toString())
                    onJobsListener.onApiError(it.toString())

                }
                )


    }


    fun doLogin(base: Base, username: String, password: String, lang: Int, onLoginListener: OnLoginListener) {
        val user = MutableLiveData<ApiResponse>()
        disposable = Api.login(username, password, lang).subscribeOn(Schedulers.io()).observeOn(mainThread()).subscribe(
                {
                    Log.i("LoginAPI", Api.gson.toJson(it))
                    user.postValue(it)
                    user.observe(base, Observer {

                        onLoginListener.onLogin(it!!)
                        disposable.dispose()

                    })
                }
                , {
            Log.e("LoginAPI", it.toString())
            onLoginListener.onApiError(it.toString())
        }
        )
    }

    fun getInstaUser(token: String): MutableLiveData<Instagram> {
        val instaUser = MutableLiveData<Instagram>()
        disposable = Api.getInstaUser(token).subscribeOn(Schedulers.io()).subscribe(
                {
                    Log.i("InstagramUserAPI", Api.gson.toJson(it))
                    instaUser.postValue(it)
                },
                {
                    Log.e("InstagramUserAPI", it.toString())
                })

        return instaUser
    }


    fun getWorkHours(baseUI: BaseUI, onWorkingHoursListener: OnWorkingHoursListener) {
        val jobs = MutableLiveData<List<WorkingTime>>()
        disposable = Api.getWorkHours().subscribeOn(Schedulers.io())
                .observeOn(mainThread())
                .subscribe(
                        {
                            jobs.postValue(it)
                            Log.i("UserWorkHoursAPI", Api.gson.toJson(it))
                            jobs.observe(baseUI, Observer {
                                onWorkingHoursListener.onWorkingHoursListed(it)
                            })
                        }, {
                    Log.e("UserWorkHoursAPI", it.toString())
                    onWorkingHoursListener.onApiError(it.toString())

                }
                )


    }

    fun verifyEmployee(base: Base, onLoginListener: OnLoginListener) {

        disposable = Api.verifyEmployee().observeOn(mainThread())
                .subscribeOn(Schedulers.io()).subscribe({
                    val apiResponse = MutableLiveData<ApiResponse>()
                    apiResponse.postValue(it)
                    apiResponse.observe(base, Observer {
                        Log.i("SignUpAPI", Api.gson.toJson(it))
                        onLoginListener.onLogin(it!!)
                        disposable.dispose()
                    })
                }, {
                    onLoginListener.onApiError(it.toString())
                    Log.e("SignUpAPI", it.toString())
                })
    }

    fun signUPFreelancer(base: Base, onLoginListener: OnLoginListener) {

        disposable = Api.signUpFreelancer(base).observeOn(mainThread())
                .subscribeOn(Schedulers.io()).subscribe({
                    val apiResponse = MutableLiveData<ApiResponse>()
                    apiResponse.postValue(it)
                    apiResponse.observe(base, Observer {
                        Log.i("SignUpAPI", Api.gson.toJson(it))
                        onLoginListener.onLogin(it!!)
                        disposable.dispose()
                    })
                }, {
                    onLoginListener.onApiError(it.toString())
                    Log.e("SignUpAPI", it.toString())
                })
    }

    fun editProfile(baseUI: BaseUI, user: User, onServiceUpdateListener: OnServiceUpdateListener) {
        disposable = Api.editProfile(user).observeOn(mainThread()).subscribeOn(Schedulers.io())
                .subscribe({
                    val data = MutableLiveData<ApiResponse>()
                    data.postValue(it)
                    data.observe(baseUI, Observer {
                        onServiceUpdateListener.onServiceUpdated(it!!.status)
                        disposable.dispose()
                    })
                }, {
                    Log.e("ProfileAPI", it.toString())
                })
    }

    fun addService(base: Base, sid: String, isOffered: Int, price: String, discount: Float, onApiListener: OnApiListener) {

        disposable = Api.addService(sid, isOffered, price, discount).subscribeOn(Schedulers.io())
                .observeOn(mainThread()).subscribe(
                        {
                            val data = MutableLiveData<ApiResponse>()
                            data.postValue(it)
                            data.observe(base, Observer { apiResponse ->
                                Log.i("AddServiceAPI", Api.gson.toJson(apiResponse))
                                onApiListener.onApiResponse(apiResponse!!)
                                disposable.dispose()
                            })
                        },
                        {
                            Log.e("AddServiceAPI", it.toString())
                            onApiListener.onApiError(it.toString())
                        }
                )
    }

    fun addWorkingHrs(baseUI: BaseUI, params: HashMap<String, Any>, onServiceUpdateListener: OnServiceUpdateListener) {
        disposable = Api.addWorkingHrs(params).observeOn(mainThread()).subscribeOn(Schedulers.io()).subscribe({
            val data = MutableLiveData<ApiResponse>()
            data.postValue(it)
            data.observe(baseUI, Observer {
                Log.i("AddWorkingHoursAPI", Api.gson.toJson(it))
                onServiceUpdateListener.onServiceUpdated(it!!.status)
                disposable.dispose()
            })
        }, {
            Log.e("AddWorkingHoursAPI", it.toString())
            onServiceUpdateListener.onApiError(it.toString())
        })
    }

    fun deleteWorkHours(baseUI: BaseUI, wid: Int, onServiceUpdateListener: OnServiceUpdateListener) {
        val jobs = MutableLiveData<ApiResponse>()
        disposable = Api.deleteWorkHours(wid).subscribeOn(Schedulers.io())
                .observeOn(mainThread())
                .subscribe(
                        {
                            jobs.postValue(it)
                            Log.i("DeleteUserWorkHoursAPI", Api.gson.toJson(it))
                            jobs.observe(baseUI, Observer {
                                onServiceUpdateListener.onServiceUpdated(it!!.status)
                            })

                        }, {
                    Log.e("DeleteUserWorkHoursAPI", it.toString())
                    onServiceUpdateListener.onApiError(it.toString())

                }
                )


    }

    fun changePassword(base: Base, password: String, onServiceUpdateListener: OnServiceUpdateListener) {
        val data = MutableLiveData<ApiResponse>()
        disposable = Api.changePassword(password).subscribeOn(Schedulers.io())
                .observeOn(mainThread()).subscribe(
                        {
                            data.postValue(it)
                            data.observe(base, Observer {
                                Log.i("ChangePasswordAPI", Api.gson.toJson(it))
                                onServiceUpdateListener.onServiceUpdated(it!!.status)
                                disposable.dispose()
                            })
                        }, {
                    Log.e("ChangePasswordAPI", it.toString())
                    onServiceUpdateListener.onApiError(it.toString())
                }
                )

    }


    fun removeProviderService(baseUI: BaseUI, serviceID: String, onServiceUpdateListener: OnServiceUpdateListener) {
        val data = MutableLiveData<ApiResponse>()
        disposable = Api.removeProviderService(serviceID).subscribeOn(Schedulers.io())
                .observeOn(mainThread()).subscribe(
                        {
                            data.postValue(it)
                            data.observe(baseUI, Observer {
                                Log.i("DelProviderServiceAPI", Api.gson.toJson(it))
                                onServiceUpdateListener.onServiceUpdated(it!!.status)
                                disposable.dispose()
                            })
                        }, {
                    Log.e("DelProviderServiceAPI", it.toString())
                    onServiceUpdateListener.onApiError(it.toString())
                }
                )
    }

    fun providerStatus(baseUI: BaseUI, status: Int, latLng: LatLng, onServiceUpdateListener: OnServiceUpdateListener) {
        val data = MutableLiveData<ApiResponse>()
        disposable = Api.setStatus(status, latLng).subscribeOn(Schedulers.io())
                .observeOn(mainThread()).subscribe(
                        {
                            data.postValue(it)
                            data.observe(baseUI, Observer {
                                Log.i("StatusAPI", Api.gson.toJson(it))
                                onServiceUpdateListener.onServiceUpdated(it!!.status)
                                disposable.dispose()
                            })
                        }, {
                    Log.e("StatusAPI", it.toString())
                    onServiceUpdateListener.onApiError(it.toString())
                }
                )
    }


    fun changeLanguage(baseUI: BaseUI, lang: Int, onServiceUpdateListener: OnServiceUpdateListener) {
        val data = MutableLiveData<ApiResponse>()
        disposable = Api.changeLanguage(lang).subscribeOn(Schedulers.io())
                .observeOn(mainThread()).subscribe(
                        {
                            data.postValue(it)
                            data.observe(baseUI, Observer {
                                Log.i("ChangeLanguageAPI", Api.gson.toJson(it))
                                onServiceUpdateListener.onServiceUpdated(it!!.status)
                                disposable.dispose()
                            })
                        }, {
                    Log.e("ChangeLanguageAPI", it.toString())
                    onServiceUpdateListener.onApiError(it.toString())
                }
                )
    }

    fun bankTransfer(base: Base, bankTransfer: BankTransfer, onServiceUpdateListener: OnServiceUpdateListener) {
        val data = MutableLiveData<ApiResponse>()
        disposable = Api.bankTransfer(bankTransfer).subscribeOn(Schedulers.io())
                .observeOn(mainThread()).subscribe(
                        {
                            data.postValue(it)
                            data.observe(base, Observer {
                                Log.i("BankingAPI", Api.gson.toJson(it))
                                onServiceUpdateListener.onServiceUpdated(it!!.status)
                                disposable.dispose()
                            })
                        }, {
                    Log.e("BankingAPI", it.toString())
                    onServiceUpdateListener.onApiError(it.toString())
                }
                )
    }

    fun providerEarnings(baseUI: BaseUI, onEarningsListener: OnEarningsListener) {
        val data = MutableLiveData<Earning>()
        disposable = Api.freelancerEarnings().subscribeOn(Schedulers.io())
                .observeOn(mainThread()).subscribe(
                        {
                            data.postValue(it)
                            data.observe(baseUI, Observer {
                                Log.i("EarningsAPI", Api.gson.toJson(it))
                                onEarningsListener.onEarnings(it)
                                disposable.dispose()
                            })
                        }, {
                    Log.e("EarningsAPI", it.toString())
                    onEarningsListener.onApiError(it.toString())
                }
                )
    }

    fun fcmToken(base: Base, onServiceUpdateListener: OnServiceUpdateListener) {
        val data = MutableLiveData<ApiResponse>()
        disposable = Api.fcmToken().subscribeOn(Schedulers.io())
                .observeOn(mainThread()).subscribe(
                        {
                            data.postValue(it)
                            data.observe(base, Observer {
                                Log.i("FcmAPI", Api.gson.toJson(it))
                                onServiceUpdateListener.onServiceUpdated(it!!.status)
                                disposable.dispose()
                            })
                        }, {
                    Log.e("FcmAPI", it.toString())
                    onServiceUpdateListener.onApiError(it.toString())
                }
                )
    }

    fun getBcServices(base: Base, onBeautyClickServicesListener: OnBeautyClickServicesListener) {
        val data = MutableLiveData<BeautyClickService>()
        disposable = Api.bcServices().subscribeOn(Schedulers.io())
                .observeOn(mainThread()).subscribe(
                        {
                            data.postValue(it)
                            data.observe(base, Observer {
                                Log.i("BcServicesAPI", Api.gson.toJson(it))
                                onBeautyClickServicesListener.onServicesListed(it)
                                disposable.dispose()
                            })
                        }, {
                    Log.e("BcServicesAPI", it.toString())
                    onBeautyClickServicesListener.onApiError(it.toString())
                }
                )
    }

    fun takeOrder(baseUI: BaseUI, orderID: String, onServiceUpdateListener: OnServiceUpdateListener, acceptOrder: Boolean) {
        val data = MutableLiveData<ApiResponse>()
        disposable = Api.takeOrder(orderID, acceptOrder).subscribeOn(Schedulers.io())
                .observeOn(mainThread()).subscribe(
                        {
                            data.postValue(it)
                            data.observe(baseUI, Observer {
                                Log.i("RejectOrderAPI", Api.gson.toJson(it))
                                onServiceUpdateListener.onServiceUpdated(it!!.status)
                                disposable.dispose()
                            })
                        }, {
                    Log.e("RejectOrderAPI", it.toString())
                    onServiceUpdateListener.onApiError(it.toString())
                }
                )
    }

    fun cancelOrder(baseUI: BaseUI, orderID: String, onServiceUpdateListener: OnServiceUpdateListener) {
        val data = MutableLiveData<ApiResponse>()
        disposable = Api.cancelOrder(orderID).subscribeOn(Schedulers.io())
                .observeOn(mainThread()).subscribe(
                        {
                            data.postValue(it)
                            data.observe(baseUI, Observer {
                                Log.i("CancelOrderAPI", Api.gson.toJson(it))
                                onServiceUpdateListener.onServiceUpdated(it!!.status)
                                disposable.dispose()
                            })
                        }, {
                    Log.e("CancelOrderAPI", it.toString())
                    onServiceUpdateListener.onApiError(it.toString())
                }
                )
    }

    fun addPicture(baseUI: BaseUI, photo: File, onServiceUpdateListener: OnServiceUpdateListener) {
        val file = Utils.getCompressedFile(baseUI.context!!, photo)
        val data = MutableLiveData<ApiResponse>()
        disposable = Api.addPicture(baseUI.context!!, file).subscribeOn(Schedulers.io())
                .observeOn(mainThread()).subscribe(
                        {
                            data.postValue(it)
                            data.observe(baseUI, Observer { apiResponse ->
                                Log.i("addPictureAPI", Api.gson.toJson(apiResponse))
                                onServiceUpdateListener.onServiceUpdated(apiResponse!!.status)
                                disposable.dispose()
                            })
                        }, {
                    Log.e("addPictureAPI", it.toString())
                    onServiceUpdateListener.onApiError(it.toString())
                }
                )
    }

    fun sendProviderLocation(base: Base, location: Location, onServiceUpdateListener: OnServiceUpdateListener) {
        val data = MutableLiveData<ApiResponse>()
        disposable = Api.sendProviderLocation(location).subscribeOn(Schedulers.io())
                .observeOn(mainThread()).subscribe(
                        {
                            data.postValue(it)
                            data.observe(base, Observer { apiResponse ->
                                onServiceUpdateListener.onServiceUpdated(apiResponse!!.status)
                                disposable.dispose()
                            })
                        }, {
                    it.printStackTrace()
                    onServiceUpdateListener.onApiError(it.toString())
                }
                )
    }
}
