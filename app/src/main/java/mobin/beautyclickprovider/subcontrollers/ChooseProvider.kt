package mobin.beautyclickprovider.subcontrollers

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import io.reactivex.Observable
import kotlinx.android.synthetic.main.choose_provider.*
import mobin.beautyclickprovider.R
import mobin.beautyclickprovider.controllers.Base
import mobin.beautyclickprovider.controllers.Registration.Companion.signUpType
import mobin.beautyclickprovider.controllers.Registration.Companion.userType
import mobin.beautyclickprovider.models.Employee
import mobin.beautyclickprovider.models.Freelancer
import mobin.beautyclickprovider.models.SignUpType
import mobin.beautyclickprovider.models.UserType

class ChooseProvider : BaseUI() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.choose_provider, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        signUpType = SignUpType.chooseProvider
        //   spinnerLanguage.adapter = getSpinnerAdapter(resources.getStringArray(R.array.lang))

        //  spinnerProvider.adapter = getSpinnerAdapter(resources.getStringArray(R.array.providers))
        next.setOnClickListener {
            val fragment = when (provider) {
                is Employee -> {
                    (provider as Employee).storeCode = etCode.text.toString()
                    SignUpForm.newInstance()
                }
                else -> AddVisa.newInstance()
            }
            validate(fragment)

        }
        spinnerLanguage.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (position == 2 || position == 1) {
                    Base.switchLocale(context!!)
                    registration.recreate()
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        }
        spinnerProvider.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                provider = if (position == 1) {
                    etCode.visibility = View.VISIBLE
                    userType = UserType.Employee
                    Employee()
                } else {
                    spinnerProvider.setSelection(2)
                    userType = UserType.Freelancer
                    etCode.visibility = View.GONE
                    Freelancer()
                }
                //  registration.employeeData[0] = provider

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        }
    }

    private fun validate(baseUI: BaseUI) {
        Observable.just(provider is Employee && (provider as Employee).storeCode.isBlank())
                .subscribe {
                    if (it)
                        Toast.makeText(context, R.string.input, Toast.LENGTH_SHORT).show()
                    else registration.setFragment(baseUI)
                }
    }

    companion object {
        fun newInstance() = ChooseProvider()
    }
}