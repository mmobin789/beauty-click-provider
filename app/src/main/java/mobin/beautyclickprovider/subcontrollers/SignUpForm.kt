package mobin.beautyclickprovider.subcontrollers

import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.AdapterView
import android.widget.PopupMenu
import android.widget.TextView
import android.widget.Toast
import io.reactivex.Observable
import kotlinx.android.synthetic.main.categories.*
import kotlinx.android.synthetic.main.dob_dialog.*
import kotlinx.android.synthetic.main.signup_form.*
import kotlinx.android.synthetic.main.toolbar.*
import mobin.beautyclickprovider.R
import mobin.beautyclickprovider.controllers.Registration.Companion.signUpType
import mobin.beautyclickprovider.models.Employee
import mobin.beautyclickprovider.models.Freelancer
import mobin.beautyclickprovider.models.Instagram
import mobin.beautyclickprovider.models.SignUpType
import java.util.*

class SignUpForm : BaseUI() {
    companion object {
        fun newInstance() = SignUpForm()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.signup_form, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        signUpType = SignUpType.Form

        if (provider is Employee) {
            nameUI.visibility = View.GONE
        }

        if (registration.intent.hasExtra("fbUser")) {
            etName.setText(registration.intent.getStringExtra("name"))
            etEmail.setText(registration.intent.getStringExtra("email"))
            etName.isEnabled = false
            etEmail.isEnabled = false
        } else if (registration.intent.hasExtra("instaUser")) {
            val user = registration.intent.getParcelableExtra<Instagram.InstagramUser>("instaUser")
            etName.setText(user.fullName)
            etUsername.setText(user.username)
            etUsername.isEnabled = false
            etName.isEnabled = false
        } else if (registration.intent.hasExtra("twitterUser")) {
            etName.setText(registration.intent.getStringExtra("name"))
            etUsername.setText(registration.intent.getStringExtra("username"))
            etEmail.setText(registration.intent.getStringExtra("email"))
            etUsername.isEnabled = false
            etName.isEnabled = false
            etEmail.isEnabled = false
        }
//        categoryUI.setOnClickListener {
//            chooseCategory(1)
//        }

        signUp.setOnClickListener {
            provider.username = etUsername.text.toString()
            if (provider is Freelancer)
                (provider as Freelancer).name = etName.text.toString()
            provider.phone = etPhone.text.toString()
            provider.iqama = etIqama.text.toString()
            provider.password = etCPassword.text.toString()
            provider.email = etEmail.text.toString()


            Observable.just(provider.isValid() && etCPassword.text.toString() == etPassword.text.toString())
                    .subscribe {
                        if (it) {
                            clearInputFocus()
                            registration.userAgreement()
                        } else Toast.makeText(context, R.string.input, Toast.LENGTH_SHORT).show()
                    }

        }
        login.setOnClickListener {
            registration.onBackPressed()
        }
        chooseDOB.setOnClickListener {
            // chooseDOB(it)
            takeDOB(it)
        }
        spinnerGender.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (position > 0) {
                    chooseCategory(position)
                } else {
                    provider.category = -1
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        }

    }

    override fun onDestroyView() {
        clearInputFocus()
        super.onDestroyView()

    }

    private fun clearInputFocus() {
        etPhone.clearFocus()
        etIqama.clearFocus()
        etName.clearFocus()
        etUsername.clearFocus()
        etPassword.clearFocus()
        etCPassword.clearFocus()
    }

    private fun chooseCategory(position: Int) {
        val dialog = Dialog(context)
        dialog.setCancelable(false)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.categories)
        dialog.drawerIV.visibility = View.INVISIBLE
        dialog.header.setText(R.string.c_cat)
        val showUI: Observable<Boolean> =
                Observable.just(position == 1)
        showUI.subscribe {

            provider.gender = if (it) {
                dialog.salonUI.visibility = View.GONE
                0

            } else {
                dialog.barberUI.visibility = View.GONE
                1

            }


        }

        dialog.barberUI.setOnClickListener {
            signUp.visibility = View.VISIBLE
            provider.category = 1 // barber
            dialog.dismiss()
        }
        dialog.massageUI.setOnClickListener {
            signUp.visibility = View.VISIBLE
            provider.category = 3 // massage
            dialog.dismiss()
        }
        dialog.bathUI.setOnClickListener {
            signUp.visibility = View.VISIBLE
            provider.category = 4 // bath
            dialog.dismiss()
        }
        dialog.salonUI.setOnClickListener {
            signUp.visibility = View.VISIBLE
            provider.category = 2  // salon
            dialog.dismiss()
        }

        dialog.show()
    }

    //    private fun showFreelancerCategory(category: String) {
//    chooseCategory.text = category
//    }
    private fun takeDOB(it: View) {
        val dialog = Dialog(it.context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dob_dialog)
        dialog.show()

        dialog.dayTV.setOnClickListener {
            chooseDayMenu(it)
        }
        dialog.monthTV.setOnClickListener {
            chooseMonthMenu(it)
        }

        dialog.yearTV.setOnClickListener {
            chooseYearMenu(it)
        }

        dialog.ok.setOnClickListener {
            val day = dialog.dayTV.text
            val month = dialog.monthTV.text
            val year = dialog.yearTV.text

            Observable.just(day.length <= 2 && month.length <= 2 && year.length >= 4 && year.toString().toIntOrNull() != null).subscribe {
                if (it) {
                    val dob = "$year-$month-$day"
                    provider.dob = dob
                    dobTV.text = dob
                    dialog.dismiss()
                } else Toast.makeText(dialog.context, R.string.input, Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun chooseDayMenu(it: View) {
        val tv = it as TextView
        val popupMenu = PopupMenu(it.context, it)
        val menu = popupMenu.menu

        popupMenu.setOnMenuItemClickListener {
            tv.text = it.title.toString()
            true
        }
        val daysInCurrentMonth = Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH)
        for (i in 1..daysInCurrentMonth) {
            menu.add(i.toString())
        }

        popupMenu.show()

    }

    private fun chooseMonthMenu(it: View) {
        val tv = it as TextView
        val popupMenu = PopupMenu(it.context, it)
        val menu = popupMenu.menu

        popupMenu.setOnMenuItemClickListener {
            tv.text = it.title.toString()
            true
        }
        for (i in 1..12) {
            menu.add(i.toString())
        }

        popupMenu.show()

    }

    private fun chooseYearMenu(it: View) {
        val tv = it as TextView
        val popupMenu = PopupMenu(it.context, it)
        val menu = popupMenu.menu

        popupMenu.setOnMenuItemClickListener {
            tv.text = it.title.toString()
            true
        }
        val year = Calendar.getInstance().get(Calendar.YEAR)
        for (i in (year - 40)..year) {
            menu.add(i.toString())
        }

        popupMenu.show()

    }

    private fun chooseDOB(it: View) {

        val calendar = Calendar.getInstance()
        val dpDialog = DatePickerDialog(it.context, R.style.TimePickerTheme, DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
            val dob = year.toString() + "-" + (month + 1) + "-" + dayOfMonth.toString()
            dobTV.text = dob
            provider.dob = dob

        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
        dpDialog.show()
    }

}