package mobin.beautyclickprovider.subcontrollers

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_my_jobs.*
import mobin.beautyclickprovider.R
import mobin.beautyclickprovider.adapters.NewJobsAdapter
import mobin.beautyclickprovider.controllers.Base.Companion.beautyClickImpl
import mobin.beautyclickprovider.controllers.Base.Companion.dismissProgressBar
import mobin.beautyclickprovider.controllers.Base.Companion.showProgressBar
import mobin.beautyclickprovider.listeners.OnJobsListener
import mobin.beautyclickprovider.listeners.OnServiceUpdateListener
import mobin.beautyclickprovider.models.Job

class NewJobs : BaseUI(), OnServiceUpdateListener {
    private var adapter: NewJobsAdapter? = null
    override fun onServiceUpdated(isUpdated: Boolean) {
        val result = if (isUpdated) {
            dismissProgressBar()
            adapter!!.removeJob()
            R.string.success
        } else R.string.failed
        Toast.makeText(context, result, Toast.LENGTH_SHORT).show()
    }

    override fun onApiError(error: String) {
        Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_my_jobs, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        rv.layoutManager = LinearLayoutManager(context)
        showProgressBar()
        beautyClickImpl.getJobs(this, object : OnJobsListener {
            override fun onJobsListed(jobs: List<Job>?) {

                val list = getPendingJobs(jobs)
                if (list.isNotEmpty()) {
                    adapter = NewJobsAdapter(this@NewJobs, this@NewJobs, getPendingJobs(jobs))
                    rv.adapter = adapter
                } else showListErrorUI(R.string.not_found)
                dismissProgressBar()
            }

            override fun onApiError(error: String) {
                onApiListError(error)
            }
        })


    }

    private fun getPendingJobs(jobs: List<Job>?): MutableList<Job> {
        val pendingJobs = mutableListOf<Job>()
        jobs?.distinctBy {
            if (it.status.equals("Pending", true))
                pendingJobs.add(it)
        }

        return pendingJobs
    }

    companion object {
        fun newInstance() = NewJobs()

    }
}