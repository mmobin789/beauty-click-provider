package mobin.beautyclickprovider.subcontrollers

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_earnings.*
import mobin.beautyclickprovider.R
import mobin.beautyclickprovider.controllers.BankAccountDetails
import mobin.beautyclickprovider.controllers.Base.Companion.beautyClickImpl
import mobin.beautyclickprovider.controllers.Base.Companion.dismissProgressBar
import mobin.beautyclickprovider.controllers.Base.Companion.showProgressBar
import mobin.beautyclickprovider.listeners.OnEarningsListener
import mobin.beautyclickprovider.models.Earning

class Earnings : BaseUI() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_earnings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {


        bankSw.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                startActivity(Intent(buttonView.context, BankAccountDetails::class.java))
            }
        }
        showProgressBar()
        beautyClickImpl.providerEarnings(this, object : OnEarningsListener {
            override fun onEarnings(earning: Earning?) {
                if (earning != null) {
                    earningsW.text = "SR ${earning.totalEarnings}"
                    myJobsTV.text = "SR " + earning.myJobsEarnings
                    cancelledJobsTV.text = "SR " + earning.cancelledJobEarnings

                } else {
                    earningsUI.visibility = View.GONE
                    earningsW.setText(R.string.not_found)
                }

                dismissProgressBar()
            }

            override fun onApiError(error: String) {
                dismissProgressBar()
                earningsUI.visibility = View.GONE
                earningsW.setText(R.string.not_found)
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
            }
        })
    }

    companion object {

        fun newInstance() = Earnings()
    }
}