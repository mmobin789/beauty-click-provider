package mobin.beautyclickprovider.subcontrollers

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.esafirm.imagepicker.features.ImagePicker
import com.google.android.gms.location.*
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.synthetic.main.profile.*
import mobin.beautyclickprovider.R
import mobin.beautyclickprovider.controllers.Base.Companion.beautyClickImpl
import mobin.beautyclickprovider.controllers.Base.Companion.dismissProgressBar
import mobin.beautyclickprovider.controllers.Base.Companion.loadWithGlide
import mobin.beautyclickprovider.controllers.Base.Companion.showProgressBar
import mobin.beautyclickprovider.listeners.OnServiceUpdateListener
import mobin.beautyclickprovider.models.User
import mobin.beautyclickprovider.utils.AppStorage
import mobin.beautyclickprovider.utils.AppStorage.getUser
import mobin.beautyclickprovider.utils.Utils
import java.io.File


class Profile : BaseUI() {
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private val user = getUser()
    private var photo = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        etName.setText(user.name)
        etUsername.setText(user.username)
        etEmail.setText(user.email)
        etPhone.setText(user.phone)
        val statusP = AppStorage.getStatus()

        if (statusP)
            statusTV.setText(R.string.online)
        else statusTV.setText(R.string.offline)

        status.isChecked = statusP

        profileEditing(false)

        edit.setOnClickListener {
            profileEditing(true)
        }

        status.setOnCheckedChangeListener { buttonView, isChecked ->
            val status = if (isChecked) {
                statusTV.setText(R.string.online)
                1
            } else {
                statusTV.setText(R.string.offline)
                0
            }

            if (lat != null && lng != null) {
                showProgressBar()
                beautyClickImpl.providerStatus(this, status, LatLng(lat!!, lng!!), object : OnServiceUpdateListener {
                    override fun onServiceUpdated(isUpdated: Boolean) {
                        val result = if (isUpdated) {
                            R.string.success
                        } else
                            R.string.failed
                        AppStorage.setStatus(isUpdated)
                        Toast.makeText(context, result, Toast.LENGTH_SHORT).show()
                        dismissProgressBar()
                    }

                    override fun onApiError(error: String) {
                        Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
                        dismissProgressBar()
                    }
                })
            } else {

                Toast.makeText(context, R.string.turnOnLocation, Toast.LENGTH_SHORT).show()
                buttonView.isChecked = false
            }


        }


        if (!user.photo.isNullOrBlank())
            loadWithGlide(user.photo!!, img, true)
        if (!user.iqamafile.isNullOrBlank())
            loadWithGlide(user.iqamafile, dIv, true)
        etName.setText(user.name)
        etUsername.setText(user.username)
        etPhone.setText(user.phone)
        if (user.rating != null)
            ratingBar.rating = user.rating


        upload.setOnClickListener {
            ImagePicker.create(this).single().imageDirectory(getString(R.string.app_name))
                    .theme(R.style.AppTheme)// Activity or Fragment
                    .start()


        }



        save.setOnClickListener {
            val email = etEmail.text.toString()
            if (Utils.isValidEmail(email)) {
                profileEditing(false)
                showProgressBar()
                val updateUser = User(user.id, etName.text.toString(), etUsername.text.toString(), user.gender, etPhone.text.toString(), 0f, photo, "", "", -1, email)
                beautyClickImpl.editProfile(this, updateUser, object : OnServiceUpdateListener {
                    override fun onServiceUpdated(isUpdated: Boolean) {
                        if (isUpdated) {
                            AppStorage.saveUser(updateUser)
                            Toast.makeText(it.context, R.string.success, Toast.LENGTH_SHORT).show()
                        }
                        dismissProgressBar()
                    }

                    override fun onApiError(error: String) {
                        dismissProgressBar()
                        Toast.makeText(it.context, error, Toast.LENGTH_SHORT).show()

                    }
                })
            } else Toast.makeText(it.context, R.string.email, Toast.LENGTH_SHORT).show()
        }

        getUserLocation()

    }


    private val locationCallback = object : LocationCallback() {

        override fun onLocationResult(p0: LocationResult?) {
            if (p0 != null) {
                lat = p0.lastLocation.latitude
                lng = p0.lastLocation.longitude
            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        showProgressBar()
        val image = ImagePicker.getFirstImageOrNull(data)
        if (image != null && image.path != null)
            addPicture(image.path)
    }


    private fun addPicture(image: String) {
        showProgressBar()
        val file = File(image)
        Utils.getFileSizeInKilobytes(file)
        beautyClickImpl.addPicture(this, file, object : OnServiceUpdateListener {
            override fun onServiceUpdated(isUpdated: Boolean) {
                if (isUpdated) {
                    Toast.makeText(context, R.string.success, Toast.LENGTH_SHORT).show()
                    user.photo = image
                    loadWithGlide(image, img, true)
                    AppStorage.saveUser(user)
                }
                dismissProgressBar()
            }

            override fun onApiError(error: String) {
                dismissProgressBar()
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
            }
        })
    }

    @SuppressLint("MissingPermission")
    private fun getUserLocation() {
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context!!)
        val locationRequest = LocationRequest.create()
        locationRequest.interval = 20000
        locationRequest.fastestInterval = 10000
        locationRequest.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
        fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, null)
    }

    private fun profileEditing(enable: Boolean) {
        etUsername.isEnabled = enable
        etName.isEnabled = enable
        etPhone.isEnabled = enable
        save.isEnabled = enable
    }

    override fun onDestroy() {
        super.onDestroy()
        fusedLocationProviderClient.removeLocationUpdates(locationCallback)
    }

    companion object {
        private var lat: Double? = null
        private var lng: Double? = null
        fun newInstance() = Profile()
    }
}