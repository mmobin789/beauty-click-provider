package mobin.beautyclickprovider.subcontrollers

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.v4.app.Fragment
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import com.vansuita.pickimage.bundle.PickSetup
import com.vansuita.pickimage.dialog.PickImageDialog
import com.vansuita.pickimage.listeners.IPickResult
import kotlinx.android.synthetic.main.fragment_my_jobs.*
import mobin.beautyclickprovider.R
import mobin.beautyclickprovider.controllers.Base
import mobin.beautyclickprovider.controllers.Home
import mobin.beautyclickprovider.controllers.Registration
import mobin.beautyclickprovider.models.Provider
import mobin.beautyclickprovider.utils.Utils

abstract class BaseUI : Fragment() {

    fun getSpinnerAdapter(data: Array<String>) = ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, data)


    lateinit var registration: Registration
    lateinit var home: Home

    fun pickImage(iPickResult: IPickResult) {
        PickImageDialog.build(PickSetup().setSystemDialog(true), iPickResult).show(fragmentManager).apply {
            setOnPickCancel { dismiss() }
        }
    }

    fun onApiListError(error: String) {
        Base.dismissProgressBar()
        Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
        showListErrorUI(R.string.not_found)
    }

    fun openMapApp(lat: Double, lng: Double) {
        // Create a Uri from an intent string. Use the result to create an Intent.
        val gmmIntentUri = Uri.parse("geo:$lat,$lng?z=12&q=$lat,$lng(${getString(R.string.b_a)})")

// Create an Intent from gmmIntentUri. Set the action to ACTION_VIEW
        val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
// Make the Intent explicit by setting the Google Maps package
        mapIntent.setPackage("com.google.android.apps.maps")

        if (mapIntent.resolveActivity(context!!.packageManager) != null)
        // Attempt to start an activity that can handle the Intent
            startActivity(mapIntent)
        else Toast.makeText(context, R.string.d_map, Toast.LENGTH_SHORT).show()
    }

    fun showListErrorUI(error: String) {
        errorTV.text = error
        errorTV.visibility = View.VISIBLE
    }

    fun showListErrorUI(@StringRes resID: Int) {
        errorTV.setText(resID)
        errorTV.visibility = View.VISIBLE
    }

    companion object {
        lateinit var provider: Provider
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (activity is Registration)
            registration = activity as Registration
        else home = activity as Home
        Utils.isNetworkConnected(context!!)
    }


}