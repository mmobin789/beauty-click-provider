package mobin.beautyclickprovider.subcontrollers

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_my_jobs.*
import mobin.beautyclickprovider.R
import mobin.beautyclickprovider.adapters.ReviewsAdapter
import mobin.beautyclickprovider.controllers.Base.Companion.beautyClickImpl
import mobin.beautyclickprovider.controllers.Base.Companion.dismissProgressBar
import mobin.beautyclickprovider.controllers.Base.Companion.showProgressBar
import mobin.beautyclickprovider.listeners.OnReviewsListener
import mobin.beautyclickprovider.models.Review

class Reviews : BaseUI(), Observer<List<Review>> {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_my_jobs, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        rv.layoutManager = LinearLayoutManager(context)
        showProgressBar()
        beautyClickImpl.getReviews(this, object : OnReviewsListener {
            override fun onApiError(error: String) {
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
                showListErrorUI(R.string.not_found)
                dismissProgressBar()
            }

            override fun onReviews(reviews: List<Review>?) {
                if (reviews != null && reviews.isNotEmpty())
                    rv.adapter = ReviewsAdapter(reviews)
                else showListErrorUI(R.string.not_found)
                dismissProgressBar()
            }
        })

    }

    override fun onChanged(t: List<Review>?) {

    }

    companion object {
        private var homeMenu: Reviews? = null
        fun newInstance(): Reviews {
            if (homeMenu == null)
                homeMenu = Reviews()
            return homeMenu!!
        }
    }
}