package mobin.beautyclickprovider.subcontrollers

import android.app.Dialog
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Toast
import kotlinx.android.synthetic.main.add_new_entry_time.*
import kotlinx.android.synthetic.main.fragment_work_hours.*
import mobin.beautyclickprovider.R
import mobin.beautyclickprovider.adapters.ViewHolder
import mobin.beautyclickprovider.adapters.WorkingHoursAdapter
import mobin.beautyclickprovider.controllers.Base.Companion.beautyClickImpl
import mobin.beautyclickprovider.controllers.Base.Companion.dismissProgressBar
import mobin.beautyclickprovider.controllers.Base.Companion.showProgressBar
import mobin.beautyclickprovider.listeners.OnListItemClickListener
import mobin.beautyclickprovider.listeners.OnServiceUpdateListener
import mobin.beautyclickprovider.listeners.OnWorkingHoursListener
import mobin.beautyclickprovider.models.WorkingTime

class WorkingHours : BaseUI() {
    private val list: MutableList<WorkingTime> = mutableListOf()
    private lateinit var adapter: WorkingHoursAdapter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_work_hours, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        rv.layoutManager = LinearLayoutManager(context)
        adapter = WorkingHoursAdapter(list)
        rv.adapter = adapter
        adapter.onListItemClickListener = object : OnListItemClickListener {
            override fun onListItemClicked(viewHolder: ViewHolder) {
                showProgressBar()
                beautyClickImpl.deleteWorkHours(this@WorkingHours, list[viewHolder.adapterPosition].wid, object : OnServiceUpdateListener {
                    override fun onServiceUpdated(isUpdated: Boolean) {
                        dismissProgressBar()
                        if (isUpdated) {
                            list.removeAt(viewHolder.adapterPosition)
                            adapter.notifyItemRemoved(viewHolder.adapterPosition)


                        } else Toast.makeText(context, R.string.not_found, Toast.LENGTH_SHORT).show()
                    }

                    override fun onApiError(error: String) {
                        Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
                    }
                })


            }
        }




        addNewTV.setOnClickListener {
            if (adapter.itemCount < 8)
                addNewEntryDialog()

        }
        showProgressBar()
        beautyClickImpl.getWorkHours(this, object : OnWorkingHoursListener {
            override fun onWorkingHoursListed(workingHours: List<WorkingTime>?) {


                if (workingHours != null && workingHours.isNotEmpty()) {
                    errorTV.visibility = View.GONE
                    list.clear()
                    list.addAll(workingHours)
                    adapter.notifyItemRangeInserted(0, workingHours.size)
                    dismissProgressBar()
                } else {
                    showListErrorUI(R.string.not_found)
                }
            }

            override fun onApiError(error: String) {
                onApiListError(error)
            }
        })
    }

    private fun addNewEntryDialog() {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.add_new_entry_time)

        dialog.addNew.setOnClickListener {
            val spinnerTo = dialog.spinnerAMTo.selectedItem.toString()
            val spinnerFrom = dialog.spinnerAMFrom.selectedItem.toString()
            val hrFrom = dialog.spinnerHRFrom.selectedItem.toString()
            val minFrom = dialog.spinnerMinutesFrom.selectedItem.toString()
            val hrTo = dialog.spinnerHRTo.selectedItem.toString()
            val minTo = dialog.spinnerMinutesTo.selectedItem.toString()
            val from = "$hrFrom:$minFrom $spinnerFrom"
            val to = "$hrTo:$minTo $spinnerTo"
            val day = dialog.spinnerDay.selectedItem.toString()
            val fromS = "$hrFrom:$minFrom"
            val toS = "$hrTo:$minTo"
            if (from != to) {
                if (sameDayExists(day))
                    Toast.makeText(it.context, R.string.day_present, Toast.LENGTH_SHORT).show()
                else {
                    val time = "$from to $to"
                    //   val timeFrom = Utils.formatDateToTime(from)
                    //  val timeTo = Utils.formatDateToTime(to)
                    //       if (timeTo == timeFrom || timeTo.before(timeFrom)) {
                    //         Toast.makeText(it.context, R.string.error_hr, Toast.LENGTH_SHORT).show()
                    //    } else {
                    showProgressBar()
                    val params = hashMapOf<String, Any>()
                    params["from_time"] = fromS
                    params["to_time"] = toS
                    params["from_ampm"] = spinnerFrom
                    params["to_ampm"] = spinnerTo
                    params["day"] = day
                    beautyClickImpl.addWorkingHrs(this, params, object : OnServiceUpdateListener {
                        override fun onApiError(error: String) {
                            dismissProgressBar()
                            Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
                        }

                        override fun onServiceUpdated(isUpdated: Boolean) {
                            dismissProgressBar()
                            val result = if (isUpdated)
                                R.string.success
                            else R.string.failed
                            Toast.makeText(context, result, Toast.LENGTH_SHORT).show()


                        }
                    })
                    val workingTime = WorkingTime(-1, day, from, to)
                    list.add(workingTime)
                    adapter.notifyItemInserted(list.size - 1)
                    dialog.dismiss()
                }
            }


        }

        dialog.show()

    }

    private fun sameDayExists(day: String): Boolean {
        var exists = false
        list.distinctBy {
            if (it.day == day)
                exists = true

        }
        return exists
    }

    companion object {
        fun newInstance(): WorkingHours = WorkingHours()

    }
}