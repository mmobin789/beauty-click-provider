package mobin.beautyclickprovider.subcontrollers

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_home.*
import mobin.beautyclickprovider.R
import mobin.beautyclickprovider.utils.AppStorage.getUser

class HomeMenu : BaseUI() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        serviceTV.setOnClickListener {
            home.setToolbarTitle(getString(R.string.services))
            home.setBaseFragment(ProviderServices.newInstance())
        }
        review.setOnClickListener {
            home.setToolbarTitle(getString(R.string.reviews))
            home.setBaseFragment(Reviews.newInstance())
        }
        pendingUI.setOnClickListener {
            home.setToolbarTitle(getString(R.string.newJob))
            home.setBaseFragment(NewJobs.newInstance())
        }

        completedUI.setOnClickListener {
            home.setToolbarTitle(getString(R.string.history))
            home.setBaseFragment(JobHistory.newInstance())
        }
        val user = getUser()
        pendingTV.text = user.jobsPending
        completedTV.text = user.jobsCompleted
        val rating = if (user.rating != null)
            user.rating
        else 0f
        ratingBar.rating = rating
    }

    companion object {
        private var homeMenu: HomeMenu? = null
        fun newInstance(): HomeMenu {
            if (homeMenu == null)
                homeMenu = HomeMenu()
            return homeMenu!!
        }
    }
}