package mobin.beautyclickprovider.subcontrollers

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.vansuita.pickimage.listeners.IPickResult
import io.reactivex.Observable
import kotlinx.android.synthetic.main.add_visa_iqama.*
import mobin.beautyclickprovider.R
import mobin.beautyclickprovider.controllers.Base.Companion.loadWithGlide
import mobin.beautyclickprovider.controllers.Registration.Companion.signUpType
import mobin.beautyclickprovider.models.Freelancer
import mobin.beautyclickprovider.models.SignUpType
import java.io.File

class AddVisa : BaseUI() {


    companion object {
        fun newInstance() = AddVisa()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.add_visa_iqama, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val freelancer = provider as Freelancer
        var photo = ""
        var iqamaFile = ""
        signUpType = SignUpType.addDocuments
        addPhoto.setOnClickListener {
            pickImage(IPickResult {
                loadWithGlide(it.path, photoIV, true)
                photo = it.path

            })
        }
        addIqama.setOnClickListener {
            pickImage(IPickResult {
                loadWithGlide(it.path, iqamaIV, false)
                iqamaFile = it.path
            })
        }
        next.setOnClickListener {
            freelancer.photo = File(photo)
            freelancer.iqamaFile = File(iqamaFile)
            Observable.just(iqamaFile.isNotBlank() && photo.isNotBlank())
                    .subscribe {
                        if (it) {
                            photo = ""
                            iqamaFile = ""
                            registration.setFragment(SignUpForm.newInstance())
                        } else Toast.makeText(context, R.string.input, Toast.LENGTH_SHORT).show()
                    }

        }

        //fetch images from social media
//        if (registration.intent.hasExtra("instaUser")) {
//            val user = registration.intent.getParcelableExtra<Instagram.InstagramUser>("instaUser")
//            photo = user.profilePic
//            loadWithGlide(user.profilePic, photoIV, true)
//
//
//        } else if (registration.intent.hasExtra("twitterUser")) {
//            photo = registration.intent.getStringExtra("picture")
//            loadWithGlide(photo, photoIV, true)
//
//        } else if (registration.intent.hasExtra("fbUser")) {
//            photo = registration.intent.getStringExtra("picture").toString()
//            loadWithGlide(photo, photoIV, true)
//        }
        //    loadFileFromUrl(photo, freelancer)


    }

//    private fun loadFileFromUrl(url: String, freelancer: Freelancer) {
//        GlideApp.with(context!!).asFile().load(url).circleCrop().into(object : SimpleTarget<File>() {
//            override fun onResourceReady(resource: File, transition: Transition<in File>?) {
//                freelancer.socialImage = resource
//                Log.i("SocialImagePath", resource.path)
//            }
//
//        })
//    }
}