package mobin.beautyclickprovider.subcontrollers

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_my_jobs.*
import mobin.beautyclickprovider.R
import mobin.beautyclickprovider.adapters.JobsHistoryAdapter
import mobin.beautyclickprovider.controllers.Base.Companion.beautyClickImpl
import mobin.beautyclickprovider.controllers.Base.Companion.dismissProgressBar
import mobin.beautyclickprovider.controllers.Base.Companion.showProgressBar
import mobin.beautyclickprovider.listeners.OnJobsListener
import mobin.beautyclickprovider.models.Job

class JobHistory : BaseUI() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_my_jobs, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        rv.layoutManager = LinearLayoutManager(context)
        showProgressBar()
        beautyClickImpl.getJobs(this, object : OnJobsListener {
            override fun onJobsListed(jobs: List<Job>?) {

                val list = getRejectedAndCompletedJobs(jobs)
                if (list.isNotEmpty()) {
                    rv.adapter = JobsHistoryAdapter(list)
                } else showListErrorUI(R.string.not_found)
                dismissProgressBar()
            }

            override fun onApiError(error: String) {
                onApiListError(error)
            }
        })


    }

    private fun getRejectedAndCompletedJobs(jobs: List<Job>?): List<Job> {
        val list = mutableListOf<Job>()
        jobs?.distinctBy {
            if (it.status.equals("Cancelled", true) || it.status.equals("Completed", true))
                list.add(it)
        }
        return list.toList()
    }

    companion object {
        private var homeMenu: JobHistory? = null
        fun newInstance(): JobHistory {
            if (homeMenu == null)
                homeMenu = JobHistory()
            return homeMenu!!
        }
    }
}