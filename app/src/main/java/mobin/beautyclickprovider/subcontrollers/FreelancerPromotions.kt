package mobin.beautyclickprovider.subcontrollers

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_work_hours.*
import mobin.beautyclickprovider.R
import mobin.beautyclickprovider.adapters.PromotionsAdapter
import mobin.beautyclickprovider.controllers.AddFreelancerPromotion
import mobin.beautyclickprovider.controllers.Base.Companion.beautyClickImpl
import mobin.beautyclickprovider.listeners.OnApiListener
import mobin.beautyclickprovider.models.ApiResponse

class FreelancerPromotions : BaseUI() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_work_hours, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        rv.layoutManager = LinearLayoutManager(context)


        addNewTV.setOnClickListener {
            home.startActivityForResult(Intent(it.context, AddFreelancerPromotion::class.java), 4)
        }

        getPromotions()

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        getPromotions()
    }

    private fun getPromotions() {
        errorTV.setText(R.string.loadingTxt)
        errorTV.visibility = View.VISIBLE
        beautyClickImpl.getPromotions(this, object : OnApiListener {
            override fun onApiResponse(apiResponse: ApiResponse) {
                if (apiResponse.status && apiResponse.promotions != null && apiResponse.promotions.isNotEmpty()) {
                    showListErrorUI("")
                    rv.adapter = PromotionsAdapter(this@FreelancerPromotions, apiResponse.promotions.toMutableList())
                } else showListErrorUI(R.string.not_found)

            }

            override fun onApiError(error: String) {
                onApiListError(error)
            }
        })
    }

    companion object {
        private var homeMenu: FreelancerPromotions? = null
        fun newInstance(): FreelancerPromotions {
            if (homeMenu == null)
                homeMenu = FreelancerPromotions()
            return homeMenu!!
        }
    }
}