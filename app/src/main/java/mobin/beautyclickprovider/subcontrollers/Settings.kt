package mobin.beautyclickprovider.subcontrollers

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.settings.*
import mobin.beautyclickprovider.R
import mobin.beautyclickprovider.controllers.Base
import mobin.beautyclickprovider.controllers.Base.Companion.beautyClickImpl
import mobin.beautyclickprovider.controllers.Base.Companion.showProgressBar
import mobin.beautyclickprovider.controllers.BeautyClickServices
import mobin.beautyclickprovider.controllers.ChangePassword
import mobin.beautyclickprovider.controllers.Login
import mobin.beautyclickprovider.listeners.OnServiceUpdateListener
import mobin.beautyclickprovider.utils.AppStorage


class Settings : BaseUI() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.settings, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        bcServices.setOnClickListener {
            startActivity(Intent(it.context, BeautyClickServices::class.java))
        }

        lang.setOnClickListener {
            showProgressBar()
            beautyClickImpl.changeLanguage(this, Base.switchLocale(context!!), object : OnServiceUpdateListener {
                override fun onServiceUpdated(isUpdated: Boolean) {
                    val result = if (isUpdated) {
                        R.string.success
                    } else
                        R.string.failed

                    Toast.makeText(context, result, Toast.LENGTH_SHORT).show()

                    if (result == R.string.success)
                        home.recreate()
                }

                override fun onApiError(error: String) {
                    Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
                }
            })

        }
        password.setOnClickListener {
            startActivity(Intent(it.context, ChangePassword::class.java))
        }
        logout.setOnClickListener {
            AppStorage.clearSession()
            home.finish()
            val login = Intent(it.context, Login::class.java)
            login.flags = Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT
            startActivity(login)

        }
    }

    companion object {
        fun newInstance(): Settings = Settings()
    }
}