package mobin.beautyclickprovider.subcontrollers

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_my_jobs.*
import mobin.beautyclickprovider.R
import mobin.beautyclickprovider.adapters.ProviderServicesAdapter
import mobin.beautyclickprovider.controllers.Base.Companion.beautyClickImpl
import mobin.beautyclickprovider.controllers.Base.Companion.dismissProgressBar
import mobin.beautyclickprovider.controllers.Base.Companion.showProgressBar
import mobin.beautyclickprovider.listeners.OnServicesListener
import mobin.beautyclickprovider.models.ProviderService

class ProviderServices : BaseUI() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_my_jobs, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        rv.layoutManager = LinearLayoutManager(context)
        showProgressBar()
        beautyClickImpl.getServices(this, object : OnServicesListener {
            override fun onServicesListed(services: ProviderService?) {


                if (services != null && services.status) {
                    rv.setItemViewCacheSize(services.services.size)
                    rv.adapter = ProviderServicesAdapter(this@ProviderServices, services.services.toMutableList())
                } else showListErrorUI(R.string.not_found)
                dismissProgressBar()
            }

            override fun onApiError(error: String) {
                onApiListError(error)
            }
        })


    }


    companion object {
        private var homeMenu: ProviderServices? = null
        fun newInstance(): ProviderServices {
            if (homeMenu == null)
                homeMenu = ProviderServices()
            return homeMenu!!
        }
    }
}