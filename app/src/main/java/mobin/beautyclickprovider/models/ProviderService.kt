package mobin.beautyclickprovider.models

data class ProviderService(

        val status: Boolean, val pid: Int, val services: List<Data>
) {
    data class Data(val service: String,

                    val id: Int,

                    val isOffered: Boolean,

                    val price: Float,

                    val discount: Float
    )
}