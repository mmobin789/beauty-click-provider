package mobin.beautyclickprovider.models

import com.google.gson.annotations.SerializedName

data class User(@SerializedName("user_id") val id: String, val name: String, val username: String, val gender: String, @SerializedName("mobile") val phone: String, val rating: Float?,
                @SerializedName("profile_pic") var photo: String?, val iqamafile: String?, val provider: String,
                @SerializedName("service_category") val categoryID: Int, val email: String
) {

    @SerializedName("completed_orders")
    val jobsCompleted = "0"
    @SerializedName("pending_orders")
    val jobsPending = "0"
}
