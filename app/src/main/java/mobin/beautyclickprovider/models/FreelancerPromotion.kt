package mobin.beautyclickprovider.models

import android.os.Parcelable
import com.google.gson.Gson
import kotlinx.android.parcel.Parcelize
import mobin.beautyclickprovider.utils.AppStorage.getUser

@Parcelize
data class FreelancerPromotion(val title: String, val price: Float, val day: String, val services: List<Service>?) : Parcelable {
    val id = -1

    val pid = getUser().id

    @Parcelize
    data class Service(val id: Int, val name: String) : Parcelable

    fun toJSON() = Gson().toJson(this)

}