package mobin.beautyclickprovider.models

import java.io.File

class Freelancer : Provider() {

    var iqamaFile: File? = null
    var photo: File? = null
    var name = ""

    override fun isValid() = super.isValid() && gender != -1 && iqamaFile != null && name.isNotBlank()
            && photo != null


    override fun toString(): String {
        return "$iqama/$username/$name/$phone/$gender/$dob/$password/$iqamaFile/$photo/$category/${email.replace("@", "_")}".replace(" ", "")
    }
}
