package mobin.beautyclickprovider.models

import mobin.beautyclickprovider.utils.AppStorage.getUser

data class BankTransfer(
        val accountName: String, val accountNumber: Int, val swift: String, val amount: Float
) {
    override fun toString(): String {
        return "${getUser().id}/$accountName/$accountNumber/$swift/$amount"
    }
}