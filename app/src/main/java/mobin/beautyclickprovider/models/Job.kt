package mobin.beautyclickprovider.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Job(
        val jobRating: Float,

        @field:SerializedName("user type")
        val userType: String?,

        val jobPostDate: String?,

        val jobPostTime: String?,

        val timeSlot: String?,

        val city: String?,

        @field:SerializedName("user_id")
        val userId: String,

        @field:SerializedName("service")
        val service: String,

        @field:SerializedName("price")
        val price: String,

        @field:SerializedName("service_id")
        val serviceId: String,

        @field:SerializedName("order id")
        val orderId: String,

        @field:SerializedName("customer_id")
        val customerId: String,

        @field:SerializedName("status")
        val status: String,

        @field:SerializedName("username")
        val username: String,

        val customer: String?,

        val lat: Double?,
        val lng: Double?

) : Parcelable