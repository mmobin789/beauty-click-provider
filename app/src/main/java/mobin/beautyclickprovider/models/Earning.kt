package mobin.beautyclickprovider.models

data class Earning(
        val pid: Int, val totalEarnings: Float, val myJobsEarnings: Float, val cancelledJobEarnings: Float
)