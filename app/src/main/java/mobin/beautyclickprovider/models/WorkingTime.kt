package mobin.beautyclickprovider.models

import com.google.gson.annotations.SerializedName

data class WorkingTime(val wid: Int, val day: String, val from: String, val to: String) {
    @SerializedName("user_id")
    var uid = 3
}