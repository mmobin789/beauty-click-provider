package mobin.beautyclickprovider.models


import com.google.gson.annotations.SerializedName


data class Review(

        @SerializedName("review id")
        val id: String,
        @SerializedName("review title")
        val title: String,
        @SerializedName("review desc")
        val description: String,
        @SerializedName("user type")
        val userType: String,
        @SerializedName("user_id")
        val uid: String,
        @SerializedName("customer_id")
        val cid: String,
        @SerializedName("customer name")
        val customerName: String,
        val rating: String,
        @SerializedName("date_added")
        val dateAdded: String


)