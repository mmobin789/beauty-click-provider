package mobin.beautyclickprovider.models

data class ApiResponse(val status: Boolean, val message: String?,
                       val user: User, val language: Int, val promotions: List<FreelancerPromotion>?)

