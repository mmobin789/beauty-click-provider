package mobin.beautyclickprovider.models

import mobin.beautyclickprovider.utils.Utils

open class Provider {
    var iqama: String = ""
    var phone: String = ""
    var dob: String = ""
    var category = -1
    //  var socialImage: File? = null
    var username: String = ""
    var password: String = ""
    var gender = -1
    var email = ""


    open fun isValid() = Utils.isValidEmail(email) && iqama.isNotBlank() && phone.isNotBlank() && dob.isNotBlank() && category != -1 && username.isNotBlank() && password.isNotBlank()


}