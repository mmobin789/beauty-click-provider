package mobin.beautyclickprovider.models

import com.google.gson.annotations.SerializedName

data class BeautyClickService(
        @SerializedName("category_id") val categoryID: String,
        val services: List<Data>?


) {
    data class Data(val id: String, val service: String, val price: String)
}