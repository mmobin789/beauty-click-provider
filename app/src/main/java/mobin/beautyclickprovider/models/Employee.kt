package mobin.beautyclickprovider.models

class Employee : Provider() {

    var storeCode = ""


    override fun toString(): String {
        return "$storeCode/$iqama/$username/$phone/$password/$dob/$gender/$category/${email.replace("@", "_")}".replace(" ", "")
    }


    override fun isValid() = super.isValid() && storeCode.isNotBlank()
}