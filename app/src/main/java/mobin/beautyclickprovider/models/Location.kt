package mobin.beautyclickprovider.models

import com.google.gson.Gson

data class Location(val lat: Double, val lng: Double) {
    fun toJSON() = Gson().toJson(this)
}