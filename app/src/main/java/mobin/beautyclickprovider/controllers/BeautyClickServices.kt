package mobin.beautyclickprovider.controllers

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_beauty_click_services.*
import kotlinx.android.synthetic.main.toolbar.*
import mobin.beautyclickprovider.R
import mobin.beautyclickprovider.adapters.BCServicesAdapter
import mobin.beautyclickprovider.listeners.OnBeautyClickServicesListener
import mobin.beautyclickprovider.models.BeautyClickService

class BeautyClickServices : Base(), OnBeautyClickServicesListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_beauty_click_services)

        init()
    }

    private fun init() {
        setToolbarTitle(getString(R.string.app_name))
        drawerIV.visibility = View.INVISIBLE
        rv.layoutManager = LinearLayoutManager(this)
        showProgressBar()
        beautyClickImpl.getBcServices(this, this)

    }

    override fun onServicesListed(beautyClickService: BeautyClickService?) {
        dismissProgressBar()
        if (beautyClickService?.services != null && beautyClickService.services.isNotEmpty()) {
            rv.setItemViewCacheSize(beautyClickService.services.size)
            rv.adapter = BCServicesAdapter(this, beautyClickService.services)
        } else {
            errorTV.setText(R.string.not_found)
            errorTV.visibility = View.VISIBLE
        }
    }

    override fun onApiError(error: String) {
        dismissProgressBar()
        errorTV.visibility = View.VISIBLE
        errorTV.setText(R.string.not_found)
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show()
    }


}
