package mobin.beautyclickprovider.controllers

import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.twitter.sdk.android.core.*
import com.twitter.sdk.android.core.identity.TwitterAuthClient
import com.twitter.sdk.android.core.models.User
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.Observables
import kotlinx.android.synthetic.main.activity_login.*
import mobin.beautyclickprovider.R
import mobin.beautyclickprovider.listeners.OnInstagramLoginListener
import mobin.beautyclickprovider.listeners.OnLoginListener
import mobin.beautyclickprovider.models.ApiResponse
import mobin.beautyclickprovider.models.Instagram
import mobin.beautyclickprovider.utils.AppStorage
import java.util.*

class Login : Base(), OnInstagramLoginListener, OnLoginListener {

    private var callbackManager: CallbackManager? = null
    private var client: TwitterAuthClient? = null
    private var loginType = Login.facebook
    private var disposable: Disposable? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        init()


    }

    private fun homeScreen() {
        startActivity(Intent(this, Home::class.java))
    }

    private fun setLanguage(position: Int) {
        val systemLang = Locale.getDefault().language
        if (position == 1 && systemLang != "en") {
            lang = switchLocale(this@Login)
            recreate()
        } else if (position == 2 && systemLang != "ar") {
            lang = switchLocale(this@Login)
            recreate()
        }
    }

    private fun autoLogin() {
        if (AppStorage.isLoggedIn()) {
            onBackPressed()
            homeScreen()
        }
    }

    private fun init() {
        hasLocationPermission(this)
        autoLogin()
        signUp.setOnClickListener {
            startActivity(Intent(it.context, Registration::class.java))
        }
        login.setOnClickListener {
            validateThenLogin()
            //homeScreen()
        }

        spinnerLanguage.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                setLanguage(position)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

        }
        loginInsta.setOnClickListener {
            loginInstagram(it)
        }
        loginFB.setOnClickListener {
            fbLogin()
        }
        loginTwitter.setOnClickListener {
            twitterLogin(it)
        }

        Log.i("lang", lang.toString())
    }

    private fun validateThenLogin() {
        val name = etName.text.toString()
        val password = etPassword.text.toString()
        val nameObservable =
                Observable.just(name)
                        .map {
                            it.isNotBlank()
                        }
        val passwordObservable = Observable.just(password)
                .map {
                    it.isNotBlank()
                }

        val result: Observable<Boolean> = Observables.combineLatest(nameObservable, passwordObservable) { n, p -> n && p }
        disposable = result.subscribe {
            if (it) {
                showProgressBar()
                //  homeScreen()
                beautyClickImpl.doLogin(this, name, password, lang, this)
            } else
                Toast.makeText(this, R.string.input, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onLogin(apiResponse: ApiResponse) {
        if (apiResponse.status) {
            AppStorage.saveUser(apiResponse.user)
            homeScreen()
        } else
            Toast.makeText(this, apiResponse.message, Toast.LENGTH_SHORT).show()
        dismissProgressBar()

    }

    override fun onApiError(error: String) {
        dismissProgressBar()
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show()
    }


    private fun loginInstagram(v: View) {
        Instagram.loginInstagram(v.context, this)
    }

    override fun onInstagramLogin(accessToken: String) {
        Log.i("InstagramAccessToken", accessToken)
        beautyClickImpl.getInstaUser(accessToken).observe(this, Observer {
            val instaUser = it!!.data
            Log.i("Instagram", instaUser.username)
            val signUp = Intent(this, Registration::class.java)
            signUp.putExtra("instaUser", it.data)
            startActivity(signUp)
        })
    }


    private fun getFacebookProfile(loginResult: LoginResult) {
        val graphRequest = GraphRequest.newMeRequest(loginResult.accessToken) { `object`, _ ->
            Log.i("GraphAPI", `object`.toString())
            val signUp = Intent(this, Registration::class.java)
            val picture = Profile.getCurrentProfile().getProfilePictureUri(200, 200)
            signUp.putExtra("name", `object`.getString("name"))
            signUp.putExtra("email", `object`.getString("email"))
            signUp.putExtra("picture", picture.toString())
            signUp.putExtra("fbUser", true)
            startActivity(signUp)

        }
        val params = Bundle()
        params.putString("fields", "name,email,picture")
        graphRequest.parameters = params
        graphRequest.executeAsync()
    }


    private fun twitterLogin(v: View) {
        loginType = Login.twitter
        Twitter.initialize(v.context)
        client = TwitterAuthClient()
        client!!.authorize(this, object : Callback<TwitterSession>() {
            override fun success(result: Result<TwitterSession>) {
                //getTwitterEmail(result)
                val user = TwitterCore.getInstance().getApiClient(result.data).accountService.verifyCredentials(false, false, true)
                user.enqueue(object : Callback<User>() {
                    override fun success(resultUser: Result<User>) {
                        val signUp = Intent(this@Login, Registration::class.java)
                        signUp.putExtra("username", result.data.userName)
                        signUp.putExtra("name", resultUser.data.name)
                        signUp.putExtra("email", resultUser.data.email)
                        signUp.putExtra("picture", resultUser.data.profileImageUrlHttps)
                        signUp.putExtra("twitterUser", true)
                        startActivity(signUp)
                    }

                    override fun failure(exception: TwitterException) {
                        exception.printStackTrace()
                    }
                })
            }

            override fun failure(exception: TwitterException) {
                Log.e("Twitter", exception.toString())
            }
        })
    }

    private fun getTwitterEmail(login: Result<TwitterSession>) {

        client!!.requestEmail(login.data, object : Callback<String>() {
            override fun success(result: Result<String>) {
                // Do something with the result, which provides the email address
                val email = result.data
                Log.i("Twitter", email + " ${login.data.userName}")
            }

            override fun failure(exception: TwitterException) {
                // Do something on failure
                Log.e("TwitterEmail", exception.toString())
            }
        })
    }

    private fun fbLogin() {
        callbackManager = CallbackManager.Factory.create()
        LoginManager.getInstance().apply {
            logInWithReadPermissions(this@Login, listOf("email", "public_profile"))
            registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
                override fun onError(error: FacebookException?) {
                    Log.e("Facebook", error.toString())
                }

                override fun onSuccess(result: LoginResult) {
                    getFacebookProfile(result)
                }

                override fun onCancel() {

                }

            })
        }

    }

    enum class Login {
        twitter, facebook
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (loginType) {
            Login.facebook -> {
                callbackManager!!.onActivityResult(requestCode, resultCode, data)
            }
            else -> {
                client!!.onActivityResult(requestCode, resultCode, data)
            }
        }


    }

    override fun onStop() {
        super.onStop()
        if (disposable != null)
            disposable!!.dispose()
    }

}
