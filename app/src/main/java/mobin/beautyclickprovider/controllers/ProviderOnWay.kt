package mobin.beautyclickprovider.controllers

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import com.google.android.gms.location.*
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.synthetic.main.activity_provider_on_way.*
import kotlinx.android.synthetic.main.toolbar.*
import mobin.beautyclickprovider.R
import mobin.beautyclickprovider.listeners.OnServiceUpdateListener
import mobin.beautyclickprovider.models.Job
import mobin.beautyclickprovider.models.Location
import java.util.concurrent.TimeUnit

class ProviderOnWay : Base(), OnMapReadyCallback {

    private lateinit var googleMap: GoogleMap
    private lateinit var job: Job
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_provider_on_way)
        init()
    }

    private fun init() {
        job = intent.getParcelableExtra("job")
        drawerIV.visibility = View.INVISIBLE
        setToolbarTitle(getString(R.string.way))
        val supportMap = map as SupportMapFragment
        supportMap.getMapAsync(this)
        getUserLocation()
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(p0: GoogleMap) {
        googleMap = p0
        googleMap.isMyLocationEnabled = true

    }

    private fun updateProviderLiveLocation(location: Location) {
        beautyClickImpl.sendProviderLocation(this, location, object : OnServiceUpdateListener {
            override fun onServiceUpdated(isUpdated: Boolean) {
                if (isUpdated)
                    Log.i("ProviderLocationSent", isUpdated.toString())
            }

            override fun onApiError(error: String) {

            }
        })
    }


    @SuppressLint("MissingPermission")
    private fun getUserLocation() {
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        val locationRequest = LocationRequest.create()
        locationRequest.interval = TimeUnit.SECONDS.toMillis(30)
        locationRequest.fastestInterval = TimeUnit.SECONDS.toMillis(20)
        locationRequest.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
        fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, null)
    }

    override fun onDestroy() {
        super.onDestroy()
        fusedLocationProviderClient.removeLocationUpdates(locationCallback)
    }

    private val locationCallback = object : LocationCallback() {

        override fun onLocationResult(p0: LocationResult?) {
            if (p0 != null) {
                googleMap.clear()
                val lat = p0.lastLocation.latitude
                val lng = p0.lastLocation.longitude
                drawPolyLines(googleMap, LatLng(job.lat!!, job.lng!!), LatLng(lat, lng))
               // zoomToMyLocation(googleMap,lat,lng)
                updateProviderLiveLocation(Location(lat, lng))
            }
        }

    }

}
