package mobin.beautyclickprovider.controllers

import android.app.Dialog
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.Window
import android.widget.Toast
import kotlinx.android.synthetic.main.user_agreement.*
import mobin.beautyclickprovider.R
import mobin.beautyclickprovider.listeners.OnLoginListener
import mobin.beautyclickprovider.models.ApiResponse
import mobin.beautyclickprovider.models.Employee
import mobin.beautyclickprovider.models.SignUpType
import mobin.beautyclickprovider.models.UserType
import mobin.beautyclickprovider.subcontrollers.AddVisa
import mobin.beautyclickprovider.subcontrollers.BaseUI.Companion.provider
import mobin.beautyclickprovider.subcontrollers.ChooseProvider

class Registration : Base() {


    companion object {
        lateinit var userType: UserType
        lateinit var signUpType: SignUpType
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)
        setFragment(ChooseProvider.newInstance())


    }


    override fun onBackPressed() {
        if (signUpType == SignUpType.chooseProvider)
            super.onBackPressed()
        else if (signUpType == SignUpType.Form && userType == UserType.Freelancer)
            setFragment(AddVisa.newInstance())
        else setFragment(ChooseProvider.newInstance())

    }

    fun userAgreement() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.user_agreement)
        dialog.agreeTV.movementMethod = ScrollingMovementMethod.getInstance()
        dialog.agreeCB.setOnCheckedChangeListener { _, isChecked ->
            dialog.ok.isEnabled = isChecked


        }
        dialog.ok.setOnClickListener {
            dialog.dismiss()
            signUp()

        }
        dialog.show()
    }

    fun signUp() {
        showProgressBar()

        val loginListener = object : OnLoginListener {
            override fun onLogin(apiResponse: ApiResponse) {

                dismissProgressBar()
                val status = if (apiResponse.status) {
                    //AppStorage.saveUser(apiResponse.user)
                    // startActivity(Intent(this@Registration, Home::class.java))
                    finish()
                    getString(R.string.success)
                } else if (!apiResponse.message.isNullOrBlank()) {
                    apiResponse.message

                } else getString(R.string.failed)

                Toast.makeText(this@Registration, status, Toast.LENGTH_SHORT).show()
            }

            override fun onApiError(error: String) {
                dismissProgressBar()
                Toast.makeText(this@Registration, error, Toast.LENGTH_SHORT).show()
            }
        }
        if (provider is Employee)
            beautyClickImpl.verifyEmployee(this, loginListener)
        else beautyClickImpl.signUPFreelancer(this, loginListener)


    }

}
