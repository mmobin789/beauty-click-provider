package mobin.beautyclickprovider.controllers

import android.os.Bundle
import android.view.View
import android.widget.Toast
import io.reactivex.Observable
import kotlinx.android.synthetic.main.activity_bank_account_details.*
import kotlinx.android.synthetic.main.toolbar.*
import mobin.beautyclickprovider.R
import mobin.beautyclickprovider.listeners.OnServiceUpdateListener
import mobin.beautyclickprovider.models.BankTransfer

class BankAccountDetails : Base() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bank_account_details)

        init()
    }

    private fun init() {
        setToolbarTitle(getString(R.string.bank_transfer))
        drawerIV.visibility = View.INVISIBLE
        withdraw.setOnClickListener {
            val accNo = etAccount.text.toString()
            val name = etName.text.toString()
            val code = etSwift.text.toString()
            val amount = etDeposit.text.toString()
            Observable.just(accNo.isNotBlank() && name.isNotBlank() && code.isNotBlank() && amount.isNotBlank() && amount.toFloat() > 0)
                    .subscribe {
                        if (it) {
                            showProgressBar()
                            val bankTransfer = BankTransfer(name, accNo.toInt(), code, amount.toFloat())
                            beautyClickImpl.bankTransfer(this, bankTransfer, object : OnServiceUpdateListener {
                                override fun onApiError(error: String) {
                                    Toast.makeText(this@BankAccountDetails, error, Toast.LENGTH_SHORT).show()
                                    dismissProgressBar()
                                }

                                override fun onServiceUpdated(isUpdated: Boolean) {
                                    val result = if (isUpdated) {
                                        R.string.success
                                    } else
                                        R.string.failed

                                    Toast.makeText(this@BankAccountDetails, result, Toast.LENGTH_SHORT).show()
                                    dismissProgressBar()
                                }
                            })
                        } else
                            Toast.makeText(this, R.string.input, Toast.LENGTH_SHORT).show()
                    }
        }


    }
}
