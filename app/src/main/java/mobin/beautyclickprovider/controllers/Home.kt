package mobin.beautyclickprovider.controllers

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.toolbar.*
import mobin.beautyclickprovider.R
import mobin.beautyclickprovider.listeners.OnServiceUpdateListener
import mobin.beautyclickprovider.subcontrollers.*
import mobin.beautyclickprovider.utils.AppStorage.getUser
import mobin.beautyclickprovider.utils.LanguageController

class Home : Base(), OnServiceUpdateListener {
    private lateinit var baseUI: BaseUI

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        drawer()
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LanguageController.onAttach(newBase!!))
    }

    private fun drawer() {
        setToolbarTitle(getString(R.string.home))
        drawerIV.setOnClickListener {
            if (drawerL.isDrawerOpen(Gravity.START))
                drawerL.closeDrawer(Gravity.START, true)
            else drawerL.openDrawer(Gravity.START, true)
        }
        val user = getUser()
        if (user.provider.equals("Employee", true)) {
            design_navigation_view.menu.findItem(R.id.nav_payment).isVisible = false
        } else {
            design_navigation_view.menu.findItem(R.id.nav_w_hrs).isVisible = false
            design_navigation_view.menu.findItem(R.id.nav_prom).isVisible = true
        }
        val ui = design_navigation_view.getHeaderView(0) as LinearLayout
        val name = ui.getChildAt(0) as TextView
        val userTV = ui.getChildAt(1) as TextView
        name.text = user.name
        userTV.text = user.username

        design_navigation_view.setNavigationItemSelectedListener {
            val header: String
            baseUI =
                    when (it.itemId) {
                        R.id.nav_completed_job -> {
                            header = getString(R.string.history)
                            JobHistory.newInstance()
                        }
                        R.id.nav_settings -> {
                            header = getString(R.string.settings)
                            Settings.newInstance()

                        }
                        R.id.nav_new_job -> {
                            header = getString(R.string.newJob)
                            NewJobs.newInstance()

                        }
                        R.id.nav_scheduled -> {
                            header = getString(R.string.scJobs)
                            MyJobs.newInstance()
                        }
                        R.id.nav_w_hrs -> {
                            header = getString(R.string.work_h)
                            WorkingHours.newInstance()
                        }
                        R.id.nav_profile -> {
                            header = getString(R.string.acc)
                            Profile.newInstance()
                        }
                        R.id.nav_payment -> {
                            header = getString(R.string.earnings)
                            Earnings.newInstance()
                        }
                        R.id.nav_about -> {
                            header = getString(R.string.about)
                            AboutUs.newInstance()
                        }
                        R.id.nav_prom -> {
                            header = getString(R.string.my_promo)
                            FreelancerPromotions.newInstance()
                        }
                        R.id.nav_phone -> {
                            val call = Intent(Intent.ACTION_DIAL, Uri.parse("tel:920026110"))
                            startActivity(call)
                            header = getString(R.string.home)
                            HomeMenu.newInstance()

                        }
                        else -> {
                            header = getString(R.string.home)
                            HomeMenu.newInstance()
                        }
                    }
            drawerL.closeDrawer(Gravity.START)
            setToolbarTitle(header)
            setFragment(baseUI)
            true
        }
//        filter.setOnClickListener {
//            startActivity(Intent(it.context, Filter::class.java))
//        }
        baseUI = HomeMenu.newInstance()
        setFragment(baseUI)

        beautyClickImpl.fcmToken(this, this)
    }

    override fun onServiceUpdated(isUpdated: Boolean) {

        Log.i("FcmAPI", isUpdated.toString())
    }

    override fun onApiError(error: String) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show()

    }

    fun setBaseFragment(baseUI: BaseUI) {
        this.baseUI = baseUI
        setFragment(this.baseUI)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        baseUI.onActivityResult(requestCode, resultCode, data)
    }

    override fun onBackPressed() {
        if (baseUI is HomeMenu)
            super.onBackPressed()
        else {
            setToolbarTitle(getString(R.string.home))
            setBaseFragment(HomeMenu.newInstance())
        }
    }
}
