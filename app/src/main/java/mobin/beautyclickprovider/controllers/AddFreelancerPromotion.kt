package mobin.beautyclickprovider.controllers

import android.app.Activity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import android.widget.PopupMenu
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_add_freelancer_promotion.*
import kotlinx.android.synthetic.main.toolbar.*
import mobin.beautyclickprovider.R
import mobin.beautyclickprovider.adapters.ProviderServicesSelectorAdapter
import mobin.beautyclickprovider.adapters.ViewHolder
import mobin.beautyclickprovider.listeners.OnApiListener
import mobin.beautyclickprovider.listeners.OnServiceClickListener
import mobin.beautyclickprovider.listeners.OnServicesListener
import mobin.beautyclickprovider.models.ApiResponse
import mobin.beautyclickprovider.models.FreelancerPromotion
import mobin.beautyclickprovider.models.ProviderService

class AddFreelancerPromotion : Base() {
    private var day = ""
    private var viewMode = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_freelancer_promotion)
        init()
    }

    private fun init() {
        filter.visibility = View.INVISIBLE
        drawerIV.visibility = View.GONE
        setToolbarTitle(getString(R.string.my_promo))
        rv.layoutManager = LinearLayoutManager(this)
        viewMode = intent.getBooleanExtra("view", false)

        if (viewMode) {
            selectedPackagePromotionUI()

        } else {
            addFreelancerPromotionUI()
        }

    }

    private fun selectedPackagePromotionUI() {
        val promotion = intent.getParcelableExtra<FreelancerPromotion>("package")
        etName.setText(promotion.title)
        etPrice.setText(promotion.price.toString())
        etName.isEnabled = false
        etPrice.isEnabled = false
        dayTV.text = promotion.day
        label.visibility = View.GONE
        errorTV.visibility = View.GONE
        add.visibility = View.INVISIBLE
        val list = getViewModeList(promotion.services!!)
        rv.adapter = ProviderServicesSelectorAdapter(list, viewMode)
    }

    private fun addFreelancerPromotionUI() {
        var service: FreelancerPromotion.Service?
        val servicesSelected = mutableListOf<FreelancerPromotion.Service>()
        beautyClickImpl.getServices(this, object : OnServicesListener {
            override fun onServicesListed(services: ProviderService?) {
                if (services != null && services.status) {
                    rv.setItemViewCacheSize(services.services.size)
                    val adapter = ProviderServicesSelectorAdapter(services.services, false)
                    rv.adapter = adapter
                    rv.visibility = View.VISIBLE
                    errorTV.visibility = View.GONE
                    adapter.onServiceClickListener = object : OnServiceClickListener {

                        override fun onListItemClicked(viewHolder: ViewHolder) {
                            val data = services.services[viewHolder.adapterPosition]
                            service = FreelancerPromotion.Service(data.id, data.service)
                            servicesSelected.add(service!!)

                        }

                        override fun onServiceUnSelected(position: Int) {
                            if (servicesSelected.isNotEmpty()) {
                                val data = services.services[position]
                                service = FreelancerPromotion.Service(data.id, data.service)
                                servicesSelected.remove(service!!)
                            }

                        }
                    }
                } else {
                    errorTV.setText(R.string.not_found)
                    errorTV.visibility = View.VISIBLE
                }
                dismissProgressBar()
            }

            override fun onApiError(error: String) {
                errorTV.text = error
                errorTV.visibility = View.VISIBLE
            }
        })

        add.setOnClickListener {
            val promotionTitle = etName.text.toString()
            val promotionPrice = etPrice.text.toString()
            if (promotionTitle.isBlank() || promotionPrice.isBlank() || day.isBlank()) {
                Toast.makeText(it.context, R.string.input, Toast.LENGTH_SHORT).show()
            } else {
                val freelancerPromotion = FreelancerPromotion(promotionTitle, promotionPrice.toFloat(), day, servicesSelected)
                Log.i("Promotion", freelancerPromotion.toJSON())
                addPromotion(freelancerPromotion)
            }
        }

        dayTV.setOnClickListener {
            chooseDayMenu(it)
        }
    }

    private fun getViewModeList(data: List<FreelancerPromotion.Service>): List<ProviderService.Data> {
        val list = mutableListOf<ProviderService.Data>()
        data.distinctBy {
            list.add(ProviderService.Data(it.name, it.id, true, -1f, -1f))
        }
        return list
    }

    private fun chooseDayMenu(view: View) {
        val popupMenu = PopupMenu(view.context, view)
        val menu = popupMenu.menu
        val daysArray = resources.getStringArray(R.array.days)
        for (day in daysArray)
            menu.add(day)
        popupMenu.show()
        popupMenu.setOnMenuItemClickListener {
            dayTV.text = it.title
            day = it.title.toString()
            true
        }
    }

    private fun addPromotion(freelancerPromotion: FreelancerPromotion) {
        showProgressBar()
        beautyClickImpl.addPromotion(this, object : OnApiListener {
            override fun onApiResponse(apiResponse: ApiResponse) {
                dismissProgressBar()
                val result = if (apiResponse.status) {
                    setResult(Activity.RESULT_OK)
                    onBackPressed()
                    R.string.success
                } else {
                    R.string.failed
                }
                Toast.makeText(this@AddFreelancerPromotion, result, Toast.LENGTH_SHORT).show()
            }

            override fun onApiError(error: String) {
                dismissProgressBar()
                Toast.makeText(this@AddFreelancerPromotion, error, Toast.LENGTH_SHORT).show()
            }
        }, freelancerPromotion)
    }


}
