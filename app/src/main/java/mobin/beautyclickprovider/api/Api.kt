package mobin.beautyclickprovider.api

import android.content.Context
import android.util.Log
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import io.reactivex.Observable
import mobin.beautyclickprovider.controllers.Base
import mobin.beautyclickprovider.models.*
import mobin.beautyclickprovider.subcontrollers.BaseUI.Companion.provider
import mobin.beautyclickprovider.utils.AppStorage.getFCM
import mobin.beautyclickprovider.utils.AppStorage.getUser
import mobin.beautyclickprovider.utils.Utils
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit


object Api {
    private const val baseUrl = "http://www.beautyclickk.com/api_beautyclick/"
    private var retrofit: Retrofit? = null
    val gson = Gson()

    private fun getWebApi(): WebService {
        if (retrofit == null) {
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            val httpClient = OkHttpClient.Builder()
                    .connectTimeout(40, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .addInterceptor(loggingInterceptor).build()
            retrofit = Retrofit.Builder().baseUrl(baseUrl)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient)
                    .build()
        }

        return retrofit!!.create(WebService::class.java)
    }

    private fun getImageRequestBody(context: Context, file: File, key: String): MultipartBody.Part {
        val compressed = Utils.getCompressedFile(context, file)
        Log.d("compressedImage", compressed.path)
        val mFile = RequestBody.create(MediaType.parse("image/*"), compressed)
        return MultipartBody.Part.createFormData(key, compressed.name, mFile)
    }


    private fun getTextRequestBody(data: String): RequestBody {
        return RequestBody.create(MediaType.parse("text/plain"), data)
    }

    fun getReviews() = getWebApi().getReview(baseUrl + "reviews/${getUser().id}")

    fun getServices() = getWebApi().getServices(getUser().id)


    fun getJobs() =
            getWebApi().getJobs(baseUrl + "orders/${getUser().id}")

    fun getWorkHours() = getWebApi().getDailyWorkHours(baseUrl + "workhours_uid/${getUser().id}")

    fun deleteWorkHours(wid: Int) = getWebApi().deleteWorkHours(baseUrl + "workhours_delete/" + wid)

    fun getInstaUser(token: String) =
            getWebApi().getInstagramUser(token)

    fun login(username: String, password: String, lang: Int): Observable<ApiResponse> {
        // val url = baseUrl + "login/$username/$password/$lang"
        val map = hashMapOf<String, Any>()
        map["username"] = username
        map["password"] = password
        map["language"] = lang
        return getWebApi().login(map)
    }

    fun addService(sid: String, isOffered: Int, price: String, discount: Float): Observable<ApiResponse> {
        val map = hashMapOf<String, Any>()
        map["provider_id"] = getUser().id
        map["service_id"] = sid
        map["is_offered"] = isOffered
        map["price"] = price
        map["discount"] = discount
        return getWebApi().addService(map)
    }

    fun addWorkingHrs(params: HashMap<String, Any>): Observable<ApiResponse> {
        params["provider_id"] = getUser().id
        return getWebApi().addWorkingHours(params)
    }

    fun editProfile(user: User): Observable<ApiResponse> {
        val map = hashMapOf<String, Any>()
        map["user_id"] = user.id
        map["email"] = user.email
        map["name"] = user.username
        map["phone"] = user.phone
        map["status"] = 1
        return getWebApi().updateProfile(map)
    }

    fun verifyEmployee(): Observable<ApiResponse> {
        val map = hashMapOf<String, Any>()
        val employee = provider as Employee
        map["code"] = employee.storeCode
        map["iqama"] = employee.iqama
        map["username"] = employee.username
        map["password"] = employee.password
        map["category_id"] = employee.category
        //    val urlPage =  //if (provider is Employee)
        //          "signup_employee"
        // else "signup_freelancer"
        //  val url = baseUrl + "$urlPage/${Base.lang}/$provider"
        //Log.i("SignUpUrl", url)
        return getWebApi().verifyEmployee(map)

    }


    fun signUpFreelancer(context: Context): Observable<ApiResponse> {
        val freelancer = provider as Freelancer
        return getWebApi().signUpFreelancer(Base.lang.toString(), freelancer.iqama,
                freelancer.username, freelancer.name, freelancer.phone
                , freelancer.gender.toString(), freelancer.dob, freelancer.password
                , freelancer.category.toString(), freelancer.email,
                getImageRequestBody(context, freelancer.photo!!, "photo"), getImageRequestBody(context, freelancer.iqamaFile!!, "iqama_attach"))
    }

    fun changePassword(password: String) = getWebApi().api(baseUrl + "upd_password/${getUser().id}/$password")

    fun removeProviderService(serviceID: String) =
            getWebApi().api(baseUrl + "del_provider_service/${getUser().id}/$serviceID")

    fun changeLanguage(lang: Int): Observable<ApiResponse> {
        val map = hashMapOf<String, Any>()
        map["language_id"] = lang
        map["user_id"] = getUser().id
        return getWebApi().changeLanguage(map)
    }

    fun setStatus(status: Int, latLng: LatLng) = getWebApi().api(baseUrl + "user_status/${getUser().id}/$status/${latLng.latitude}/${latLng.longitude}")


    fun bankTransfer(bankTransfer: BankTransfer) = getWebApi().api(baseUrl + "ins_bank/$bankTransfer")

    fun freelancerEarnings() = getWebApi().providerEarnings(baseUrl + "provider_earning/${getUser().id}")

    fun fcmToken() = getWebApi().api(baseUrl + "fcm_provider/${getUser().id}/${getFCM()}")

    fun bcServices() = getWebApi().getBeautyClickServices(baseUrl + "services_cid/${getUser().categoryID}")

    fun takeOrder(orderID: String, acceptOrder: Boolean): Observable<ApiResponse> {
        val order = if (acceptOrder) "accept"
        else "reject"
        return getWebApi().api(baseUrl + "${order}_order/${getUser().id}/$orderID")
    }

    fun cancelOrder(orderID: String) = getWebApi().api(baseUrl + "cancel_order/${getUser().id}/$orderID")

    fun addPicture(context: Context, photo: File) = getWebApi().addFreelancerPhoto(baseUrl + "add_profile", getTextRequestBody(getUser().id), getImageRequestBody(context, photo, "photo"))

    fun getPromotions() = getWebApi().api(baseUrl + "get_promotions/${getUser().id}")

    fun deletePromotion(id: Int) = getWebApi().api(baseUrl + "delete_promotion/$id")

    fun addPromotion(freelancerPromotion: FreelancerPromotion) = getWebApi().addPromotion(freelancerPromotion)


    fun sendProviderLocation(location: Location): Observable<ApiResponse> {
        val map = hashMapOf<String, Any>()
        map["user_id"] = getUser().id
        map["lat"] = location.lat
        map["lng"] = location.lng
        return getWebApi().sendProviderLocation(map)
    }

}
