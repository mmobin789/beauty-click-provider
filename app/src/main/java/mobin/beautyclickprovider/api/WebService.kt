package mobin.beautyclickprovider.api

import io.reactivex.Observable
import mobin.beautyclickprovider.models.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface WebService {
    @FormUrlEncoded
    @POST("providerLogin")
    fun login(@FieldMap hashMap: HashMap<String, Any>): Observable<ApiResponse>

    @Multipart
    @POST("signup_freelancer")
    fun signUpFreelancer(@Part("language") lang: String, @Part("iqama") iqama: String,
                         @Part("username") username: String, @Part("name") name: String,
                         @Part("phone") phone: String, @Part("gender") gender: String,
                         @Part("dob") dob: String, @Part("password") password: String,
                         @Part("category") category: String, @Part("email") email: String,
                         @Part photo: MultipartBody.Part, @Part iqama_attach: MultipartBody.Part): Observable<ApiResponse>


    @FormUrlEncoded
    @POST("verifyEmployee")
    fun verifyEmployee(@FieldMap hashMap: HashMap<String, Any>): Observable<ApiResponse>


    @Multipart
    @POST
    fun addFreelancerPhoto(@Url url: String, @Part("pid") pid: RequestBody, @Part photo: MultipartBody.Part): Observable<ApiResponse>

    @GET("services_uid")
    fun getServices(@Query("user_id") uid: String): Observable<ProviderService>

    @GET
    fun getJobs(@Url url: String): Observable<List<Job>>

    @GET
    fun getReview(@Url url: String): Observable<List<Review>>

    @GET
    fun getDailyWorkHours(@Url url: String): Observable<List<WorkingTime>>

    @DELETE
    fun deleteWorkHours(@Url url: String): Observable<ApiResponse>

    @GET("${Instagram.insta_BASE_URL}v1/users/self")
    fun getInstagramUser(@Query("access_token") accessToken: String): Observable<Instagram>


    @GET
    fun api(@Url url: String): Observable<ApiResponse>

    @GET
    fun providerEarnings(@Url url: String): Observable<Earning>

    @GET
    fun getBeautyClickServices(@Url url: String): Observable<BeautyClickService>

    @POST("add_promotion")
    fun addPromotion(@Body freelancerPromotion: FreelancerPromotion): Observable<ApiResponse>

    @FormUrlEncoded
    @POST("ins_provider_service")
    fun addService(@FieldMap hashMap: HashMap<String, Any>): Observable<ApiResponse>

    @FormUrlEncoded
    @POST("ins_work_hour")
    fun addWorkingHours(@FieldMap hashMap: HashMap<String, Any>): Observable<ApiResponse>

    @FormUrlEncoded
    @POST("upd_langauge")
    fun changeLanguage(@FieldMap hashMap: HashMap<String, Any>): Observable<ApiResponse>

    @FormUrlEncoded
    @POST("upd_profile")
    fun updateProfile(@FieldMap hashMap: HashMap<String, Any>): Observable<ApiResponse>

    @FormUrlEncoded
    @POST("setLatLng")
    fun sendProviderLocation(@FieldMap hashMap: HashMap<String, Any>): Observable<ApiResponse>
}