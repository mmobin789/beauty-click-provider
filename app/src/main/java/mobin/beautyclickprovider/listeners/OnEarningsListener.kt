package mobin.beautyclickprovider.listeners

import mobin.beautyclickprovider.models.Earning

interface OnEarningsListener : OnAPIFailListener {
    fun onEarnings(earning: Earning?)
}