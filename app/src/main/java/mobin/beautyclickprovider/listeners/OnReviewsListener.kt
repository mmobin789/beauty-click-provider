package mobin.beautyclickprovider.listeners

import mobin.beautyclickprovider.models.Review

interface OnReviewsListener : OnAPIFailListener {
    fun onReviews(reviews: List<Review>?)
}