package mobin.beautyclickprovider.listeners

import mobin.beautyclickprovider.models.ApiResponse

interface OnApiListener : OnAPIFailListener {
    fun onApiResponse(apiResponse: ApiResponse)
}