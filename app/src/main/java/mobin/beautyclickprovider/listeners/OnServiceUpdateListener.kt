package mobin.beautyclickprovider.listeners

interface OnServiceUpdateListener : OnAPIFailListener {
    fun onServiceUpdated(isUpdated: Boolean)
}