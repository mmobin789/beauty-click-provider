package mobin.beautyclickprovider.listeners

interface OnServiceClickListener : OnListItemClickListener {
    fun onServiceUnSelected(position: Int)
}