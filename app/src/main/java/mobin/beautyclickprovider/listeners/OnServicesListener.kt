package mobin.beautyclickprovider.listeners

import mobin.beautyclickprovider.models.ProviderService

interface OnServicesListener : OnAPIFailListener {
    fun onServicesListed(services: ProviderService?)
}