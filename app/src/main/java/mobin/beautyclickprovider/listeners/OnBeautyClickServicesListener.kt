package mobin.beautyclickprovider.listeners

import mobin.beautyclickprovider.models.BeautyClickService

interface OnBeautyClickServicesListener : OnAPIFailListener {
    fun onServicesListed(beautyClickService: BeautyClickService?)
}