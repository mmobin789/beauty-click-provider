package mobin.beautyclickprovider.listeners

interface OnListItemLongClickListener {
    fun onLongClick(position: Int): Boolean
}