package mobin.beautyclickprovider.listeners

import mobin.beautyclickprovider.models.Job

interface OnJobsListener : OnAPIFailListener {
    fun onJobsListed(jobs: List<Job>?)
}