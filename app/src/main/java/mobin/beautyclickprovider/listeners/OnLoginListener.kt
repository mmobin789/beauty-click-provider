package mobin.beautyclickprovider.listeners

import mobin.beautyclickprovider.models.ApiResponse

interface OnLoginListener : OnAPIFailListener {
    fun onLogin(apiResponse: ApiResponse)
}