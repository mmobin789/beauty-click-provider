package mobin.beautyclickprovider.listeners

interface OnAPIFailListener {
    fun onApiError(error: String)
}