package mobin.beautyclickprovider.listeners

import mobin.beautyclickprovider.models.WorkingTime

interface OnWorkingHoursListener : OnAPIFailListener {
    fun onWorkingHoursListed(workingHours: List<WorkingTime>?)
}