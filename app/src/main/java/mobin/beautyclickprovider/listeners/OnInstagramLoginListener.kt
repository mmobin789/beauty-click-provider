package mobin.beautyclickprovider.listeners

interface OnInstagramLoginListener {
    fun onInstagramLogin(accessToken: String)
}