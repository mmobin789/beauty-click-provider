package mobin.beautyclickprovider.listeners

import mobin.beautyclickprovider.adapters.ViewHolder


interface OnListItemClickListener {
    fun onListItemClicked(viewHolder: ViewHolder)
}