package mobin.beautyclickprovider.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.new_job_row.*
import mobin.beautyclickprovider.R
import mobin.beautyclickprovider.controllers.Base.Companion.beautyClickImpl
import mobin.beautyclickprovider.controllers.Base.Companion.showProgressBar
import mobin.beautyclickprovider.listeners.OnServiceUpdateListener
import mobin.beautyclickprovider.models.Job
import mobin.beautyclickprovider.subcontrollers.BaseUI

class NewJobsAdapter(private val onServiceUpdateListener: OnServiceUpdateListener, private val baseUI: BaseUI, private val list: MutableList<Job>) : RecyclerView.Adapter<ViewHolder>() {
    private var position = 0
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val vh = ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.new_job_row, parent, false))
//        val user = AppStorage.getUser()
//        if (user.provider == "freelancer") {
//            vh.date.visibility = View.INVISIBLE
//        }
        vh.accept.setOnClickListener {
            position = vh.adapterPosition
            showProgressBar()
            beautyClickImpl.takeOrder(baseUI, list[position].orderId, onServiceUpdateListener, true)
        }
        vh.reject.setOnClickListener {
            position = vh.adapterPosition
            showProgressBar()
            beautyClickImpl.takeOrder(baseUI, list[position].orderId, onServiceUpdateListener, false)
        }

        vh.viewMap.setOnClickListener {
            position = vh.adapterPosition
            val order = list[position]
            if (order.lat != null && order.lng != null)
                baseUI.openMapApp(order.lat, order.lng)
            else Toast.makeText(it.context, R.string.not_found, Toast.LENGTH_SHORT).show()
        }
        return vh
    }

    fun removeJob() {
        list.removeAt(position)
        notifyItemRemoved(position)
        if (itemCount == 0)
            baseUI.showListErrorUI(R.string.not_found)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val job = list[position]
        holder.job.text = job.service
        holder.price.text = job.price
        val time = job.jobPostDate + " " + job.timeSlot
        holder.date.text = time
        holder.customerName.text = job.customer
        holder.time.text = job.jobPostTime


    }

}