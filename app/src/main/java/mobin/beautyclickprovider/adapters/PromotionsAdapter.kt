package mobin.beautyclickprovider.adapters

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.promo_row.*
import mobin.beautyclickprovider.R
import mobin.beautyclickprovider.controllers.AddFreelancerPromotion
import mobin.beautyclickprovider.controllers.Base.Companion.beautyClickImpl
import mobin.beautyclickprovider.controllers.Base.Companion.dismissProgressBar
import mobin.beautyclickprovider.controllers.Base.Companion.showProgressBar
import mobin.beautyclickprovider.listeners.OnApiListener
import mobin.beautyclickprovider.models.ApiResponse
import mobin.beautyclickprovider.models.FreelancerPromotion
import mobin.beautyclickprovider.subcontrollers.FreelancerPromotions

class PromotionsAdapter(private val freelancerPromotions: FreelancerPromotions, private val list: MutableList<FreelancerPromotion>) : RecyclerView.Adapter<ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val vh = ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.promo_row, parent, false))

        vh.containerView.setOnClickListener {
            val position = vh.adapterPosition
            freelancerPromotions.startActivity(Intent(it.context, AddFreelancerPromotion::class.java)
                    .putExtra("view", true).putExtra("package", list[position]))
        }

        vh.delete.setOnClickListener {
            val promotion = list[vh.adapterPosition]
            showProgressBar()
            beautyClickImpl.deletePromotion(promotion.id, freelancerPromotions, object : OnApiListener {
                override fun onApiResponse(apiResponse: ApiResponse) {
                    dismissProgressBar()
                    if (apiResponse.status) {
                        list.removeAt(vh.adapterPosition)
                        notifyItemRemoved(vh.adapterPosition)
                    } else Toast.makeText(freelancerPromotions.context, R.string.not_found, Toast.LENGTH_SHORT).show()
                }

                override fun onApiError(error: String) {
                    dismissProgressBar()
                    Toast.makeText(freelancerPromotions.context, error, Toast.LENGTH_SHORT).show()
                }
            })
        }
        return vh
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val promotion = list[position]
        holder.dayTV.text = promotion.title


    }
}