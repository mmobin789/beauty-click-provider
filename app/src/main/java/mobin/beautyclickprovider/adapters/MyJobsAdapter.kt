package mobin.beautyclickprovider.adapters

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.scheduled_job_row.*
import mobin.beautyclickprovider.R
import mobin.beautyclickprovider.controllers.Base.Companion.beautyClickImpl
import mobin.beautyclickprovider.controllers.Base.Companion.dismissProgressBar
import mobin.beautyclickprovider.controllers.Base.Companion.showProgressBar
import mobin.beautyclickprovider.controllers.ProviderOnWay
import mobin.beautyclickprovider.listeners.OnServiceUpdateListener
import mobin.beautyclickprovider.models.Job
import mobin.beautyclickprovider.subcontrollers.MyJobs

class MyJobsAdapter(private val myJobs: MyJobs, private val list: MutableList<Job>) : RecyclerView.Adapter<ViewHolder>() {

    private var position = 0
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val vh = ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.scheduled_job_row, parent, false))

        vh.cancel.setOnClickListener {
            position = vh.adapterPosition
            val order = list[position]
            showProgressBar()
            beautyClickImpl.cancelOrder(myJobs, order.orderId, object : OnServiceUpdateListener {
                override fun onServiceUpdated(isUpdated: Boolean) {
                    dismissProgressBar()
                    Toast.makeText(myJobs.context, if (isUpdated) {
                        removeJob()
                        R.string.success
                    } else R.string.failed, Toast.LENGTH_SHORT).show()
                }

                override fun onApiError(error: String) {
                    dismissProgressBar()
                    Toast.makeText(myJobs.context, error, Toast.LENGTH_SHORT).show()
                }
            })
        }

        vh.viewMap.setOnClickListener {
            position = vh.adapterPosition
            val order = list[position]
            if (order.lat != null && order.lng != null)
                myJobs.openMapApp(order.lat, order.lng)
            else Toast.makeText(it.context, R.string.not_found, Toast.LENGTH_SHORT).show()
        }

        vh.way.setOnClickListener {
            position = vh.adapterPosition
            // send order lat/lng along.
            val order = list[position]
            if (order.lat != null && order.lng != null)
                myJobs.startActivity(Intent(it.context, ProviderOnWay::class.java).putExtra("job", order))
            else Toast.makeText(it.context, R.string.not_found, Toast.LENGTH_SHORT).show()

        }
        return vh
    }


    private fun removeJob() {
        list.removeAt(position)
        notifyItemRemoved(position)
        if (itemCount == 0)
            myJobs.showListErrorUI(R.string.not_found)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val job = list[position]
        holder.job.text = job.service
        holder.price.text = job.price
        val time = job.jobPostDate + " " + job.timeSlot
        holder.date.text = time
        holder.customerName.text = job.customer
        holder.loc.text = job.city

    }


}