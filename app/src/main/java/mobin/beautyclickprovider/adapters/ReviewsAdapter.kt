package mobin.beautyclickprovider.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import kotlinx.android.synthetic.main.reviews_row.*
import mobin.beautyclickprovider.R
import mobin.beautyclickprovider.models.Review

class ReviewsAdapter(private val list: List<Review>) : RecyclerView.Adapter<ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.reviews_row, parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val review = list[position]
        holder.title.text = review.title
        holder.review.text = review.description
        val sign = review.customerName + " / " + review.dateAdded
        holder.date.text = sign
        holder.ratingBar.rating = review.rating.toFloat()

    }
}