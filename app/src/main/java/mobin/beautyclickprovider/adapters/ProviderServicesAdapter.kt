package mobin.beautyclickprovider.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.Toast
import kotlinx.android.synthetic.main.services_row.*
import mobin.beautyclickprovider.R
import mobin.beautyclickprovider.controllers.Base.Companion.beautyClickImpl
import mobin.beautyclickprovider.controllers.Base.Companion.dismissProgressBar
import mobin.beautyclickprovider.controllers.Base.Companion.showProgressBar
import mobin.beautyclickprovider.listeners.OnServiceUpdateListener
import mobin.beautyclickprovider.models.ProviderService
import mobin.beautyclickprovider.subcontrollers.ProviderServices


class ProviderServicesAdapter(private val providerServices: ProviderServices, private val list: MutableList<ProviderService.Data>) : RecyclerView.Adapter<ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val vh = ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.services_row, parent, false))
        vh.checkbox.setOnClickListener {
            val cb = it as CheckBox
            if (!cb.isChecked) removeService(vh.adapterPosition)

        }
        return vh
    }


    private fun removeService(adapterPosition: Int) {
        val id = list[adapterPosition].id
        showProgressBar()
        beautyClickImpl.removeProviderService(providerServices, id.toString(), object : OnServiceUpdateListener {
            override fun onServiceUpdated(isUpdated: Boolean) {
                if (isUpdated) {
                    Toast.makeText(providerServices.context, R.string.success, Toast.LENGTH_SHORT).show()
                    list.removeAt(adapterPosition)
                    notifyItemRemoved(adapterPosition)
                }
                dismissProgressBar()
            }

            override fun onApiError(error: String) {
                dismissProgressBar()
                Toast.makeText(providerServices.context, error, Toast.LENGTH_SHORT).show()

            }
        })

    }


    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val service = list[position]
        val s = service.service.trim() + " " + holder.containerView.context.getString(R.string.discount) + " " +
                service.discount + " " + holder.containerView.context.getString(R.string.price) + " " + service.price
        holder.serviceTV.text = s
    }


}