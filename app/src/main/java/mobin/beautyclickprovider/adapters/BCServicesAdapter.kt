package mobin.beautyclickprovider.adapters

import android.app.Dialog
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.Window
import android.widget.CheckBox
import android.widget.Toast
import kotlinx.android.synthetic.main.services_row.*
import kotlinx.android.synthetic.main.set_service_dialog.*
import mobin.beautyclickprovider.R
import mobin.beautyclickprovider.controllers.Base
import mobin.beautyclickprovider.controllers.Base.Companion.beautyClickImpl
import mobin.beautyclickprovider.controllers.Base.Companion.dismissProgressBar
import mobin.beautyclickprovider.controllers.Base.Companion.showProgressBar
import mobin.beautyclickprovider.listeners.OnApiListener
import mobin.beautyclickprovider.models.ApiResponse
import mobin.beautyclickprovider.models.BeautyClickService
import mobin.beautyclickprovider.utils.AppStorage.getUser


class BCServicesAdapter(private val base: Base, private val list: List<BeautyClickService.Data>) : RecyclerView.Adapter<ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val vh = ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.services_row, parent, false))
        vh.checkbox.isChecked = false
        vh.containerView.setOnClickListener {
            vh.checkbox.isChecked = true
            val service = list[vh.adapterPosition]
            if (getUser().provider.equals("freelancer", true))
                setServiceDialog(vh.checkbox, service)
            else addProviderServiceAPI(it.context, service, service.price, 0f)
        }
        return vh
    }


    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val service = list[position]
        val s = service.service.trim() + " " + holder.containerView.context.getString(R.string.price) + " " + service.price
        holder.serviceTV.text = s
    }

    private fun addProviderServiceAPI(context: Context, service: BeautyClickService.Data, price: String?, discount: Float?) {
        if (!price.isNullOrBlank()) {
            showProgressBar()
            beautyClickImpl.addService(base, service.id, 1, price, discount!!, object : OnApiListener {
                override fun onApiResponse(apiResponse: ApiResponse) {
                    dismissProgressBar()
                    val result = if (apiResponse.status) {
                        base.getString(R.string.success)
                    } else if (!apiResponse.message.isNullOrBlank())
                        apiResponse.message
                    else base.getString(R.string.failed)
                    Toast.makeText(context, result, Toast.LENGTH_SHORT).show()
                }

                override fun onApiError(error: String) {
                    Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
                    dismissProgressBar()
                }
            })

        }
    }

    private fun setServiceDialog(checkBox: CheckBox, service: BeautyClickService.Data) {
        val dialog = Dialog(checkBox.context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.set_service_dialog)
        dialog.service.text = service.service.trim()
        dialog.ok.setOnClickListener {

            var discount = dialog.etDiscount.text.toString().toFloatOrNull()
            if (discount == null)
                discount = 0f
            val price = dialog.etPrice.text.toString()

            addProviderServiceAPI(dialog.context, service, price, discount)

        }
        dialog.setOnCancelListener {
            checkBox.isChecked = false
        }
        dialog.show()
    }

}