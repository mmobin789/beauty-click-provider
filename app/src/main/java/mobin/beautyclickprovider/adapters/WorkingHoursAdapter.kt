package mobin.beautyclickprovider.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import kotlinx.android.synthetic.main.working_hr_row.*
import mobin.beautyclickprovider.R
import mobin.beautyclickprovider.listeners.OnListItemClickListener
import mobin.beautyclickprovider.models.WorkingTime


class WorkingHoursAdapter(private val list: MutableList<WorkingTime>) : RecyclerView.Adapter<ViewHolder>() {
    lateinit var onListItemClickListener: OnListItemClickListener
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val vh = ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.working_hr_row, parent, false))
        vh.delete.setOnClickListener {
            onListItemClickListener.onListItemClicked(vh)

        }
        return vh
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val workAssignment = list[position]
        holder.dayTV.text = workAssignment.day
        val timeSlot = workAssignment.from + " to " + workAssignment.to
        holder.timeTV.text = timeSlot
    }

}