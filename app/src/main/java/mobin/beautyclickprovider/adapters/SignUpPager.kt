package mobin.beautyclickprovider.adapters

import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import mobin.beautyclickprovider.subcontrollers.AddVisa
import mobin.beautyclickprovider.subcontrollers.BaseUI
import mobin.beautyclickprovider.subcontrollers.ChooseProvider
import mobin.beautyclickprovider.subcontrollers.SignUpForm

class SignUpPager(fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager) {
    override fun getItem(position: Int): BaseUI {

        return when (position) {
            1 -> {
                SignUpForm.newInstance()
            }
            2 -> {
                AddVisa.newInstance()
            }
            else -> {
                ChooseProvider.newInstance()

            }
        }

    }

    override fun getCount(): Int {
        return 3
    }

}