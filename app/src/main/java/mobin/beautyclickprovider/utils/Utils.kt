package mobin.beautyclickprovider.utils

import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.ConnectivityManager
import android.os.Environment
import android.util.Base64
import android.util.Log
import android.util.Patterns
import android.widget.Toast
import id.zelory.compressor.Compressor
import mobin.beautyclickprovider.R
import java.io.*
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.text.SimpleDateFormat
import java.util.*


object Utils {


    fun encodeImage(path: String): String {
        val bitmap = BitmapFactory.decodeFile(path)
        val baos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos)
        val bytes = baos.toByteArray()
        return Base64.encodeToString(bytes, Base64.DEFAULT)
    }

    fun isValidEmail(email: String?): Boolean {
        return !email.isNullOrBlank() && Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    fun getCompressedFile(context: Context, file: File): File {
        Log.i("CompressiveFilePath", file.path)


        try {
            return Compressor(context)
                    .setMaxWidth(200)
                    .setMaxHeight(200)
                    //  .setQuality(60)
                    .setCompressFormat(Bitmap.CompressFormat.PNG)
                    .setDestinationDirectoryPath(Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_PICTURES).absolutePath)
                    .compressToFile(file)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return file
    }

    fun getFileSizeInKilobytes(file: File) = Log.i("ImageSize", (file.length() / 1024).toString() + " KB")

    fun formatDate(date: Date): String {
        val sdf = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
        return sdf.format(date)
    }

    fun getCurrentTime(): String {
        val sdf = SimpleDateFormat("hh:mm a", Locale.getDefault())
        return sdf.format(Date())
    }

    fun isNetworkConnected(context: Context) {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (cm.activeNetworkInfo == null)
            Toast.makeText(context, R.string.no_net, Toast.LENGTH_SHORT).show()

    }

    fun formatDateToTime(date: String): Date {
        val sdf = SimpleDateFormat("hh:mm a", Locale.getDefault())
        return sdf.parse(date)
    }

    fun getKeyHash(context: Context): String {
        var keyhash = ""

        try {
            @SuppressLint("PackageManagerGetSignatures") val info = context.packageManager.getPackageInfo(
                    context.packageName,
                    PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                keyhash = Base64.encodeToString(md.digest(), Base64.DEFAULT)
                Log.d("KeyHash:", keyhash)
            }
        } catch (ignored: PackageManager.NameNotFoundException) {

        } catch (ignored: NoSuchAlgorithmException) {
        }

        return keyhash
    }

    fun readFileFromAssets(fileName: String, context: Context): String {
        val returnString = StringBuilder()
        var fIn: InputStream? = null
        var isr: InputStreamReader? = null
        var input: BufferedReader? = null
        try {
            fIn = context.resources.assets
                    .open(fileName, Context.MODE_PRIVATE)
            isr = InputStreamReader(fIn)
            input = BufferedReader(isr)
            val line = input.readLine()
            while (line != null) {
                returnString.append(line)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            try {
                if (isr != null)
                    isr.close()
                if (fIn != null)
                    fIn.close()
                if (input != null)
                    input.close()
            } catch (e2: Exception) {
                e2.printStackTrace()
            }

        }
        return returnString.toString()
    }
}