package mobin.beautyclickprovider.utils

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import mobin.beautyclickprovider.api.Api
import mobin.beautyclickprovider.models.User

object AppStorage {
    private var prefs: SharedPreferences? = null
    private val gson = Api.gson
    private const val keyUser = "user"
    fun init(context: Context) {
        if (prefs == null)
            prefs = PreferenceManager.getDefaultSharedPreferences(context)
    }

    fun saveUser(user: User) {
        val s = gson.toJson(user)
        prefs!!.edit().putString(keyUser, s).apply()
    }

    fun getFCM() = prefs!!.getString("fcm", "")

    fun setFCM(token: String) = prefs!!.edit().putString("fcm", token).apply()

    fun getUser() = gson.fromJson(prefs!!.getString(keyUser, ""), User::class.java)

    fun clearSession() = prefs!!.edit().clear().apply()

    fun isLoggedIn() = prefs!!.getString(keyUser, "").isNotBlank()

    fun getStatus() = prefs!!.getBoolean("status", false)

    fun setStatus(enabled: Boolean) = prefs!!.edit().putBoolean("status", enabled).apply()
}